
import time
import argparse
import os.path
from core import Renderer


parser = argparse.ArgumentParser()
parser.add_argument("json_file")
args = parser.parse_args()

fname = args.json_file
full_path = os.path.realpath(fname)

print("Begin loading %s scene description file." % full_path)
start_time = time.clock()
renderer = Renderer.parse_json(full_path)
end_time = time.clock()
print("Loading took %f seconds." % (end_time - start_time))
print("Rendering ...")
start_time = time.clock()
while renderer.run_iterations():
    print("Pass %i, elapsed time %f" % (renderer.current_iteration, time.clock() - start_time))
end_time = time.clock()
print("Renderig took %f seconds." % (end_time - start_time))
print("Writing output image")
renderer.save_image()
print("Finished")


