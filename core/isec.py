
from simdy import simdy_kernel, struct, register_user_type, float32, float32x3, int32, float64x3, float64
from .params import Params, Shape, Ray, HitPoint, Light, TriangleData, Rectangle
from .bvh import isect_bool_mesh_bvh, isect_mesh_bvh
from .bvh16 import isect_mesh_bvh16, isect_bool_mesh_bvh16
from .core import calculate_normal


@simdy_kernel
def isect_linear_bool_shape(ray: Ray, shape: Shape, max_dist: float32):
    faces = shape.faces
    verts = shape.verts
    for i in range(shape.ntriangles):
        idx = i * 9
        face0 = faces[idx]
        face1 = faces[idx + 1]
        face2 = faces[idx + 2]
        p0 = verts[face0]
        p1 = verts[face1]
        p2 = verts[face2]
        e1 = p1 - p0
        e2 = p2 - p0

        d = ray.direction
        o = ray.origin

        p = float32x3(d[1] * e2[2] - d[2] * e2[1], d[2] * e2[0] - d[0] * e2[2], d[0] * e2[1] - d[1] * e2[0])
        a = dot(e1, p)
        if a > -0.00001 and a < 0.00001:
            continue
        f = 1.0 / a
        s = ray.origin - p0
        u = f * dot(s, p)
        if u < 0.0 or u > 1.0:
            continue
        q = float32x3(s[1] * e1[2] - s[2] * e1[1], s[2] * e1[0] - s[0] * e1[2], s[0] * e1[1] - s[1] * e1[0])
        v = f * dot(d, q)
        if v < 0.0 or u + v > 1.0:
            continue

        t = f * dot(e2, q)
        if t > 0.0001 and t < max_dist:
            return 1

    return 0


@simdy_kernel
def isect_bool_shape(ray: Ray, shape: Shape, max_dist: float32):

    if shape.ntriangles > 16:
        # hit_ocur = isect_bool_mesh_bvh(ray, shape, max_dist)
        hit_ocur = isect_bool_mesh_bvh16(ray, shape, max_dist)
        return hit_ocur
    return isect_linear_bool_shape(ray, shape, max_dist)


@simdy_kernel
def isect_bool_rectangle(ray: Ray, rect: Rectangle, max_dist: float32):

    temp1 = dot(ray.direction, rect.normal)
    if temp1 == 0.0:
        return 0

    t = dot(rect.p - ray.origin, rect.normal) / temp1
    if t > 0.0001 and t < max_dist:
        p = ray.origin + ray.direction * t
        d = p - rect.p
        ddota = dot(d, rect.edge_a)
        if ddota < 0.0 or ddota > dot(rect.edge_a, rect.edge_a):
            return 0
        ddotb = dot(d, rect.edge_b)
        if ddotb < 0.0 or ddotb > dot(rect.edge_b, rect.edge_b):
            return 0
        return 1
    return 0


@simdy_kernel
def isect_bool_scene(ray: Ray, params: Params, max_dist: float32):
    shapes = params.shapes
    inv_ray_dir = float32x3(1.0) / ray.direction
    for i in range(len(shapes)):
        shp = shapes[i]
        t1 = (shp.bbox_p0 - ray.origin) * inv_ray_dir
        t2 = (shp.bbox_p1 - ray.origin) * inv_ray_dir
        tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
        tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
        if tmax > max(tmin, float32(0.0)):
            if isect_bool_shape(ray, shp, max_dist):
                return 1

    shapes = params.light_meshes
    for i in range(len(shapes)):
        shp = shapes[i]
        t1 = (shp.bbox_p0 - ray.origin) * inv_ray_dir
        t2 = (shp.bbox_p1 - ray.origin) * inv_ray_dir
        tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
        tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
        if tmax > max(tmin, float32(0.0)):
            if isect_bool_shape(ray, shp, max_dist):
                return 1

    rectangles = params.light_rectangles
    for i in range(len(rectangles)):
        rect = rectangles[i]
        if isect_bool_rectangle(ray, rect, max_dist):
            return 1
    return 0


@simdy_kernel
def isect_linear_shape(ray: Ray, shape: Shape, max_dist: float32, tdata: TriangleData):
    faces = shape.faces
    verts = shape.verts
    hit_ocur = 0

    for i in range(shape.ntriangles):
        idx = i * 9
        face0 = faces[idx]
        face1 = faces[idx + 1]
        face2 = faces[idx + 2]
        p0 = verts[face0]
        p1 = verts[face1]
        p2 = verts[face2]
        e1 = p1 - p0
        e2 = p2 - p0

        d = ray.direction
        o = ray.origin

        p = float32x3(d[1] * e2[2] - d[2] * e2[1], d[2] * e2[0] - d[0] * e2[2], d[0] * e2[1] - d[1] * e2[0])
        a = dot(e1, p)
        if a > -0.00001 and a < 0.00001:
            continue
        f = 1.0 / a
        s = ray.origin - p0
        u = f * dot(s, p)
        if u < 0.0 or u > 1.0:
            continue
        q = float32x3(s[1] * e1[2] - s[2] * e1[1], s[2] * e1[0] - s[0] * e1[2], s[0] * e1[1] - s[1] * e1[0])
        v = f * dot(d, q)
        if v < 0.0 or u + v > 1.0:
            continue

        t = f * dot(e2, q)
        if t > 0.0001 and t < max_dist:
            hit_ocur = 1
            tdata.t = t
            tdata.u = u
            tdata.v = v
            tdata.index = i
            max_dist = t

    return hit_ocur


@simdy_kernel
def isect_shape(ray: Ray, shape: Shape, max_dist: float32, tdata: TriangleData):

    if shape.ntriangles > 16:
        # hit_ocur = isect_mesh_bvh(ray, shape, max_dist, tdata)
        hit_ocur = isect_mesh_bvh16(ray, shape, max_dist, tdata)
        return hit_ocur
    return isect_linear_shape(ray, shape, max_dist, tdata)


@simdy_kernel
def isect_rectangle_light(ray: Ray, rect: Rectangle, max_dist: float32, hp: HitPoint):

    temp1 = dot(ray.direction, rect.normal)
    if temp1 == 0.0:
        return 0

    t = dot(rect.p - ray.origin, rect.normal) / temp1
    if t > 0.0001 and t < max_dist:
        p = ray.origin + ray.direction * t
        d = p - rect.p
        ddota = dot(d, rect.edge_a)
        if ddota < 0.0 or ddota > dot(rect.edge_a, rect.edge_a):
            return 0
        ddotb = dot(d, rect.edge_b)
        if ddotb < 0.0 or ddotb > dot(rect.edge_b, rect.edge_b):
            return 0

        hp.t = t
        hp.hit = p
        hp.normal = rect.normal
        hp.wo = ray.direction * -1.0
        hp.mat_id = rect.mat_id
        return 1
    return 0


@simdy_kernel
def isect_lights(ray: Ray, params: Params, max_dist: float32, hp: HitPoint):
    lights = params.lights
    hit_ocur = 0
    inv_ray_dir = float32x3(1.0) / ray.direction
    for i in range(int32(len(lights))):
        light = lights[i]
        if light.type != 1:
            continue
        if light.shp_type == 0:  # rectangle
            rect = params.light_rectangles[light.shp_id]
            if isect_rectangle_light(ray, rect, max_dist, hp):
                hit_ocur = 1
                max_dist = hp.t
                hp.light_id = i
        elif light.shp_type == 1:  # mesh
            mesh = params.light_meshes[light.shp_id]
            t1 = (mesh.bbox_p0 - ray.origin) * inv_ray_dir
            t2 = (mesh.bbox_p1 - ray.origin) * inv_ray_dir
            tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
            tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
            if tmax > max(tmin, float32(0.0)):
                tdata = TriangleData()
                if isect_shape(ray, mesh, max_dist, tdata):
                    hit_ocur = 1
                    max_dist = tdata.t
                    hp.t = tdata.t
                    hp.hit = ray.origin + tdata.t * ray.direction
                    hp.wo = ray.direction * -1.0
                    hp.normal = calculate_normal(mesh, tdata.u, tdata.v, tdata.index)
                    hp.light_tri_idx = tdata.index
                    hp.mat_id = mesh.mat_id
                    hp.light_id = i

    return hit_ocur


@simdy_kernel
def isect_scene(ray: Ray, params: Params, hp: HitPoint):
    shapes = params.shapes
    max_dist = float32(1e30)
    hit_ocur = 0
    tdata = TriangleData()
    inv_ray_dir = float32x3(1.0) / ray.direction
    for i in range(int32(len(shapes))):
        shp = shapes[i]
        t1 = (shp.bbox_p0 - ray.origin) * inv_ray_dir
        t2 = (shp.bbox_p1 - ray.origin) * inv_ray_dir
        tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
        tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
        if tmax > max(tmin, float32(0.0)):
            if isect_shape(ray, shp, max_dist, tdata):
                hit_ocur = 1
                max_dist = tdata.t
                hit_shape = shp
                tri_idx = tdata.index
                u = tdata.u
                v = tdata.v
                t = tdata.t

    if isect_lights(ray, params, max_dist, hp):
        return 1

    if hit_ocur:
        hp.t = t
        hp.hit = ray.origin + t * ray.direction
        hp.wo = ray.direction * -1.0
        hp.normal = calculate_normal(hit_shape, u, v, tri_idx)
        hp.mat_id = hit_shape.mat_id
        hp.light_id = -1

    return hit_ocur


@simdy_kernel
def isect_bool_sphere(origin: float64x3, radius: float64, ray: Ray, max_dist: float64):

    temp = float64x3(ray.origin) - origin
    b = dot(temp, float64x3(ray.direction)) * 2.0
    c = dot(temp, temp) - radius * radius
    disc = b * b - 4.0 * c
    if disc < 0:
        return 0

    e = sqrt(disc)
    t = ((b * -1) - e) * 0.5
    if t > 0.005 and t < max_dist:
        return 1

    t = ((b * -1) + e) * 0.5
    if t > 0.005 and t < max_dist:
        return 1
    return 0

