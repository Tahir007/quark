
import multiprocessing
from math import sqrt
from simdy import simdy_kernel, int32, float32, float32x3, float32x4, register_user_type, struct, float64, float64x3, float32x16, ISet
from .params import Params, Shape, HitPoint, Ray, Light, BsdfSample, LightSample
from .isec import isect_scene, isect_bool_scene
from .bsdf import bsdf, sample_bsdf, pdf_bsdf
from .light import sample_light, emission, pdf_light


# @simdy_kernel
# def pt_trace_ray(ray: Ray, params: Params):
#     hp = HitPoint()
#     hit = isect_scene(ray, params, hp)
#     color = float32x3()
#     if hit:
#         ls = LightSample()
#         is_valid = sample_light(hp, params, ls)
#         if is_valid:
#             hp.wi = ls.wi
#             color = ls.intensity * bsdf(hp, params)
#
#             bs = BsdfSample()
#             sample_bsdf(hp, params, bs)
#     return color


@simdy_kernel
def luminance(col: float32x3):
    return dot(col, float32x3(0.212671, 0.715160, 0.07216))


# @simdy_kernel
# def pt_trace_ray(ray: Ray, params: Params):
#     hp = HitPoint()
#     ls = LightSample()
#     bs = BsdfSample()
#     max_depth = 10
#     acum = float32x3(0)
#     cur_depth = 1
#     path = float32x3(1)
#
#     while 1:
#         hit = isect_scene(ray, params, hp)
#         if hit == 0:
#             break
#
#         if cur_depth >= max_depth:
#             break
#
#         is_valid = sample_light(hp, params, ls)
#         if is_valid:
#             hp.wi = ls.wi
#             acum += path * ls.intensity * bsdf(hp, params)
#
#         is_valid = sample_bsdf(hp, params, bs)
#         if is_valid == 0:
#             break
#
#         path *= bs.factor
#
#         ray.origin = hp.hit
#         ray.direction = bs.wi
#
#         if cur_depth > 3:
#             q = max(float32(0.05), 1.0 - luminance(path))
#             if random_float32() < q:
#                 break
#
#             path /= float32x3(1.0 - q)
#
#         cur_depth += 1
#
#     return acum

@simdy_kernel
def balance_heuristic(pdf: float32, other_pdf: float32):
    return pdf / (pdf + other_pdf)


@simdy_kernel
def pt_trace_ray(ray: Ray, params: Params):
    hp = HitPoint()
    ls = LightSample()
    bs = BsdfSample()
    max_depth = 10
    acum = float32x3(0)
    cur_depth = 1
    path = float32x3(1)

    hit_ocur = isect_scene(ray, params, hp)
    while 1:
        if hit_ocur == 0:
            break

        if cur_depth == 1 and hp.light_id >= 0:
            hp.wi = ray.direction
            acum += path * emission(hp, params)

        if cur_depth >= max_depth:
            break

        is_valid = sample_light(hp, params, ls)
        if is_valid:
            hp.wi = ls.wi
            if ls.delta_light:
                weight = float32(1.0)
            else:
                weight = balance_heuristic(ls.pdf, pdf_bsdf(hp, params))
            acum += path * ls.intensity * bsdf(hp, params) * weight

        is_valid = sample_bsdf(hp, params, bs)
        if is_valid == 0:
            break

        path *= bs.factor

        ray.origin = hp.hit
        ray.direction = bs.wi

        hit_ocur = isect_scene(ray, params, hp)
        if hit_ocur == 1:
            if hp.light_id >= 0:
                hp.wi = bs.wi
                weight = balance_heuristic(bs.pdf, pdf_light(hp, params))
                acum += path * emission(hp, params) * weight
        else:
            break

        if cur_depth > 3:
            q = max(float32(0.05), 1.0 - luminance(path))
            if random_float32() < q:
                break

            path /= float32x3(1.0 - q)

        cur_depth += 1

    return acum


@simdy_kernel
def mitchell1d(x: float32, B: float32, C: float32) -> float32:
    x = abs(2 * x)
    if x > 1.0:
        return ((-B - 6 * C) * x * x * x + (6 * B + 30 * C) * x * x + (-12 * B - 48 * C) * x + (8 * B + 24 * C)) * 0.1666666666
    return ((12 - 9 * B - 6 * C) * x * x * x + (-18 + 12 * B + 6 * C) * x * x + (6 - 2 * B)) * 0.166666666


@simdy_kernel
def mitchell1d_16(x: float32x16, B: float32, C: float32) -> float32x16:
    x = abs(2.0 * x)

    tmp1 = ((-B - 6 * C) * x * x * x + (6 * B + 30 * C) * x * x + (-12 * B - 48 * C) * x + float32x16(8 * B + 24 * C)) * 0.1666666666
    tmp2 = ((12 - 9 * B - 6 * C) * x * x * x + (-18 + 12 * B + 6 * C) * x * x + float32x16(6 - 2 * B)) * 0.166666666
    return select(tmp1, tmp2, x > float32x16(1.0))


@simdy_kernel
def evaluate_filter(params: Params, dx: float32, dy: float32) -> float32:
    if params.flt_type == 0 or params.flt_type == 1:
        return float32(1)
    elif params.flt_type == 2:  # tent filter
        return max(float32(0.0), params.flt_radius - abs(dx)) * max(float32(0.0), params.flt_radius - abs(dy))
    # elif params.flt_type == 3:  # gaussian
    #     alpha = float32(2.0)
    #     expXY = exp(-alpha * params.flt_radius * params.flt_radius)
    #     return max(float32(0.0), exp(-alpha * dx * dx) - expXY) * max(float32(0.0), exp(-alpha * dy * dy) - expXY)
    elif params.flt_type == 3:  # optimized gaussian
        alpha = float32(2.0)
        x0 = -alpha * params.flt_radius * params.flt_radius
        vals = exp(float32x4(x0, x0, -alpha * dx * dx, -alpha * dy * dy))
        return max(float32(0.0), vals[2] - vals[0]) * max(float32(0.0), vals[3] - vals[1])
    if params.flt_type == 4:  # mitchell
        if abs(dx) > params.flt_radius or abs(dy) > params.flt_radius:
            return float32(0)
        B = float32(1.0 / 3.0)
        C = float32(1.0 / 3.0)
        inv_radius = 1.0 / params.flt_radius
        val = mitchell1d(dx * inv_radius, B, C) * mitchell1d(dy * inv_radius, B, C)
        return val
    return float32(0)


@simdy_kernel
def evaluate_filter16(params: Params, dx: float32x16, dy: float32x16) -> float32x16:
    if params.flt_type == 0 or params.flt_type == 1:
        vals = select(float32x16(1), float32x16(0), abs(dx) <= float32x16(params.flt_radius))
        return select(vals, float32x16(0), abs(dy) <= float32x16(params.flt_radius))
    elif params.flt_type == 2:  # tent filter
        flt_radius = float32x16(params.flt_radius)
        return max(float32x16(0.0), flt_radius - abs(dx)) * max(float32x16(0.0), flt_radius - abs(dy))
    elif params.flt_type == 3:  # gaussian
        alpha = float32(2.0)
        expXY = float32x16(exp(-alpha * params.flt_radius * params.flt_radius))
        return max(float32x16(0.0), exp(-alpha * dx * dx) - expXY) * max(float32x16(0.0), exp(-alpha * dy * dy) - expXY)
    elif params.flt_type == 4:  # mitchell
        B = float32(0.333333333333)
        C = float32(0.333333333333)
        inv_radius = 1.0 / params.flt_radius
        vals = mitchell1d_16(dx * inv_radius, B, C) * mitchell1d_16(dy * inv_radius, B, C)
        vals = select(vals, float32x16(0), abs(dx) <= float32x16(params.flt_radius))
        return select(vals, float32x16(0), abs(dy) <= float32x16(params.flt_radius))
    return float32x16(0)


@simdy_kernel(nthreads=multiprocessing.cpu_count()*2)
def path_tracer(params: Params):
    width = float32(params.resx)
    height = float32(params.resy)
    aspect_ratio = width / height
    fov = params.cam_fov
    buf = params.color_buffer
    flt_radius = params.flt_radius
    for j in range(0, params.resy, params.tile_height):
        starty = j
        endy = starty + params.tile_height
        if endy > params.resy:
            endy = params.resy
        for i in range(0, params.resx, nthreads() * params.tile_width):
            startx = i + thread_idx() * params.tile_width
            endx = startx + params.tile_width
            if endx > params.resx:
                endx = params.resx

            for py in range(starty, endy):
                for px in range(startx, endx):
                    u = random_float32()
                    v = random_float32()
                    xw = (2.0 * ((float32(px) + u) / width) - 1.0) * aspect_ratio * fov
                    yw = (2.0 * ((float32(py) + v) / height) - 1.0) * fov
                    d = params.cam_u * xw + params.cam_v * yw - params.cam_w
                    d = d / float32x3(sqrt(dot(d, d)))  # normalization of vector
                    ray = Ray()
                    ray.origin = params.cam_eye
                    ray.direction = d
                    c = pt_trace_ray(ray, params)
                    if params.flt_type == 0:  # just simple box filtering - radius 0.5
                        flt_weight = float32(1.0)
                        buf[py * params.resx + px] += float32x4(c[0], c[1], c[2], flt_weight)
                    else:
                        xmin = int32(ceil(float32(px) + u - 0.5 - flt_radius))
                        xmin = max(xmin, startx)
                        xmax = int32(floor(float32(px) + u - 0.5 + flt_radius + 1.0))
                        xmax = min(xmax, endx)
                        ymin = int32(ceil(float32(py) + v - 0.5 - flt_radius))
                        ymin = max(ymin, starty)
                        ymax = int32(floor(float32(py) + v - 0.5 + flt_radius + 1.0))
                        ymax = min(ymax, endy)
                        for spy in range(ymin, ymax):
                            for spx in range(xmin, xmax):
                                flt_weight = evaluate_filter(params, float32(spx) + 0.5 - (float32(px) + u), float32(spy) + 0.5 - (float32(py) + v))
                                nc = c * flt_weight
                                buf[spy * params.resx + spx] += float32x4(nc[0], nc[1], nc[2], flt_weight)


@simdy_kernel(nthreads=multiprocessing.cpu_count()*2)
def path_tracer16(params: Params):
    width = float32(params.resx)
    height = float32(params.resy)
    aspect_ratio = width / height
    fov = params.cam_fov
    buf = params.color_buffer
    flt_radius = params.flt_radius

    for j in range(0, params.resy, params.tile_height):
        starty = j
        endy = starty + params.tile_height
        if endy > params.resy:
            endy = params.resy
        for i in range(0, params.resx, nthreads() * params.tile_width):
            startx = i + thread_idx() * params.tile_width
            endx = startx + params.tile_width
            if endx > params.resx:
                endx = params.resx

            for py in range(starty, endy):
                for px in range(startx, endx):
                    u = random_float32x16()
                    v = random_float32x16()
                    x_ndc = (float32x16(float32(px)) + u) / float32x16(width)
                    y_ndc = (float32x16(float32(py)) + v) / float32x16(height)

                    xw = (2.0 * x_ndc - float32x16(1.0)) * aspect_ratio * fov
                    yw = (2.0 * y_ndc - float32x16(1.0)) * fov

                    dx = params.cam_u[0] * xw + params.cam_v[0] * yw - float32x16(params.cam_w[0])
                    dy = params.cam_u[1] * xw + params.cam_v[1] * yw - float32x16(params.cam_w[1])
                    dz = params.cam_u[2] * xw + params.cam_v[2] * yw - float32x16(params.cam_w[2])

                    m = sqrt(dx * dx + dy * dy + dz * dz)
                    dx = dx / m
                    dy = dy / m
                    dz = dz / m
                    ray = Ray()

                    if params.flt_type == 0:  # just simple box filtering - radius 0.5
                        acum_col = float32x3()
                        for ri in range(16):
                            ray.origin = params.cam_eye
                            ray.direction = float32x3(dx[ri], dy[ri], dz[ri])
                            c = pt_trace_ray(ray, params)
                            acum_col += c

                        flt_weight = float32(16.0)
                        buf[py * params.resx + px] += float32x4(acum_col[0], acum_col[1], acum_col[2], flt_weight)
                    else:
                        xmin = int32(ceil(float32(px) - 0.5 - flt_radius))
                        xmin = max(xmin, startx)
                        xmax = int32(floor(float32(px) - 0.5 + flt_radius + 1.0))
                        xmax = min(xmax, endx)
                        ymin = int32(ceil(float32(py) - 0.5 - flt_radius))
                        ymin = max(ymin, starty)
                        ymax = int32(floor(float32(py) - 0.5 + flt_radius + 1.0))
                        ymax = min(ymax, endy)

                        colors = array(float32x3, 16)
                        for ri in range(16):
                            ray.origin = params.cam_eye
                            ray.direction = float32x3(dx[ri], dy[ri], dz[ri])
                            c = pt_trace_ray(ray, params)
                            colors[ri] = c

                        for spy in range(ymin, ymax):
                            for spx in range(xmin, xmax):
                                dx = float32x16(float32(spx)) + float32x16(0.5) - (float32x16(float32(px)) + u)
                                dy = float32x16(float32(spy)) + float32x16(0.5) - (float32x16(float32(py)) + v)
                                flt_weights = evaluate_filter16(params, dx, dy)
                                acum_col = float32x3(0.0)
                                acum_weight = float32(0.0)
                                for fi in range(16):
                                    acum_col += colors[fi] * flt_weights[fi]
                                    acum_weight += flt_weights[fi]

                                buf[spy * params.resx + spx] += float32x4(acum_col[0], acum_col[1], acum_col[2], acum_weight)
