
import json
import os.path
from math import sqrt, radians, tan
from simdy import int32, float32x3, float32, simdy_kernel, array_float32x3, array_float32x4, float32x4, float32x8
from .params import Params, Material, Light, Rectangle
from .obj import load_obj_shapes
from .bvh import create_bvh_tree_nodes
from .bvh16 import create_bvh16_tree_nodes
from .img import save_image
from .ptracer import path_tracer, path_tracer16


@simdy_kernel
def tone_map_color_buffer( input: array_float32x4, output: array_float32x3, tonemap: int32):
    inv_gamma = float32(1.0 / 2.2)
    for i in range(len(input)):
        v = input[i]
        col = float32x3(0)
        if v[3] > 0.0:
            v = v / float32x4(v[3])
            col = float32x3(v[0], v[1], v[2])
            # tonemap 0 - linear, 1 - gamma, 2 - reinhard, 3 - filmic
            if tonemap == 1:  # gamma
                col = exp(inv_gamma * log(col))
            elif tonemap == 2:  # reinhard
                col = col / (col + float32x3(1.0))
                col = exp(inv_gamma * log(col))
            elif tonemap == 3:  # filmic
                x = max(float32x3(0.0), col - float32x3(0.004))
                col = (x * (6.2 * x + float32x3(0.5))) / (x * (6.2 * x + float32x3(1.7)) + float32x3(0.06))

        col = max(float32x3(0.0), col)
        output[i] = col


@simdy_kernel
def calculate_mesh_bbox(vertices: array_float32x3) -> float32x8:
    min_p = float32x3(1e30, 1e30, 1e30)
    max_p = float32x3(-1e30, -1e30, -1e30)
    eps = float32x3(0.001)
    for i in range(len(vertices)):
        min_p = min(min_p, vertices[i] - eps)
        max_p = max(max_p, vertices[i] + eps)

    p0 = float32x4(min_p[0], min_p[1], min_p[2], float32(0))
    p1 = float32x4(max_p[0], max_p[1], max_p[2], float32(0))
    return float32x8(p0, p1)


class Renderer:
    def __init__(self):
        self._params = Params()
        self._set_default_params()
        self._current_iteration = 0
        self.output_file = None
        self._shapes = {}
        self._materials = {}

    @property
    def current_iteration(self):
        return self._current_iteration

    def _set_default_params(self):
        self.set_resolution(200, 200)
        self._params.spp = int32(1)
        data = {'eye': (2.0, 2.0, 2.0),
                'lookat': (0.0, 0.0, 0.0),
                'up': (0.0, 1.0, 0.0),
                'fov': 90.0}
        self.set_camera_params(data)
        self._params.flt_type = int32(0) # no filtering
        self._params.flt_size = float32(0.5)
        self._tonemap = "linear"

    def set_resolution(self, resx, resy):
        self._params.resx = int32(resx)
        self._params.resy = int32(resy)
        self._params.color_buffer.resize(resx * resy)
        self._params.color_buffer.zero()
        self._params.tile_width = int32(32)
        self._params.tile_height = int32(32)

    def run_iterations(self):
        if self._current_iteration >= self._params.spp:
            return False

        # path_tracer(self._params)
        # self._current_iteration += 1

        if self._current_iteration + 16 > self._params.spp:
            path_tracer(self._params)
            self._current_iteration += 1
        else:
            path_tracer16(self._params)
            self._current_iteration += 16

        return True

    def save_image(self):
        if self.output_file:
            gs = self._params
            output_buffer = array_float32x3(size=gs.resx * gs.resy)
            tonemaps = {'linear': 0, 'gamma': 1, 'reinhard': 2, 'filmic': 3}
            tonemap = int32(tonemaps[self._tonemap])
            tone_map_color_buffer(gs.color_buffer, output_buffer, tonemap)
            save_image(output_buffer, gs.resx, gs.resy, self.output_file)

    def set_global_params(self, data, dir_name):
        res = data.get('resolution', (200, 200))
        resx, resy = int(res[0]), int(res[1])
        self.set_resolution(resx, resy)
        spp = data.get('samples_per_pixel', 1)
        self._params.spp = int32(spp)

        filters = {'no_filter': 0, 'box': 1, 'tent': 2, 'gaussian': 3, 'mitchell': 4}
        flt_radiuses = {'no_filter': 0.5, 'box': 0.5, 'tent': 1, 'gaussian': 2, 'mitchell': 2}
        flt = data.get('filter', 'no_filter')
        self._params.flt_type = int32(filters[flt])
        self._params.flt_radius = float32(data.get('filter_radius', flt_radiuses[flt]))

        output = data.get('output', 'output.png')
        self.output_file = os.path.join(dir_name, output)
        tonemap = data.get('tonemap', 'linear')
        if tonemap in ('linear', 'gamma', 'reinhard', 'filmic'):
            self._tonemap = tonemap

    def set_camera_params(self, data):
        eye = data.get('eye', (2.0, 2.0, 2.0))
        eye = (float(eye[0]), float(eye[1]), float(eye[2]))
        lookat = data.get('lookat', (0.0, 0.0, 0.0))
        lookat = (float(lookat[0]), float(lookat[1]), float(lookat[2]))
        up = data.get('up', (0.0, 1.0, 0.0))
        up = (float(up[0]), float(up[1]), float(up[2]))
        fov = data.get('fov', 90.0)
        fov = float(fov)

        def normalize(v):
            length = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])
            return (v[0] / length, v[1] / length, v[2] / length)

        def cross(v1, v2):
            return (v1[1] * v2[2] - v1[2] * v2[1],
                    v1[2] * v2[0] - v1[0] * v2[2],
                    v1[0] * v2[1] - v1[1] * v2[0])

        def calculate_uvw(e, l, up):
            # singularity
            if e[0] == l[0] and e[2] == l[2] and e[1] > l[1]:  # looking vertically down
                u = float32x3(0.0, 0.0, 1.0)
                v = float32x3(1.0, 0.0, 0.0)
                w = float32x3(0.0, 1.0, 0.0)
            elif e[0] == l[0] and e[2] == l[2] and e[1] < l[1]:  # looking vertically up
                u = float32x3(1.0, 0.0, 0.0)
                v = float32x3(0.0, 0.0, 1.0)
                w = float32x3(0.0, -1.0, 0.0)
            else:
                # w is in oposite direction of view - right handed system
                w = (e[0] - l[0], e[1] - l[1], e[2] - l[2])
                w = normalize(w)
                u = cross(up, w)
                u = normalize(u)
                v = cross(w, u)
                u = float32x3(u[0], u[1], u[2])
                v = float32x3(v[0], v[1], v[2])
                w = float32x3(w[0], w[1], w[2])

            return u, v, w

        u, v, w = calculate_uvw(eye, lookat, up)
        self._params.cam_eye = float32x3(eye[0], eye[1], eye[2])
        self._params.cam_u = u
        self._params.cam_v = v
        self._params.cam_w = w
        self._params.cam_fov = float32(tan((radians(fov * 0.5))))

    def load_materials(self, materials):
        for mat_data in materials:
            mat_name = mat_data['name']
            if mat_name in self._materials:
                raise ValueError("Material %s allready exist!" % mat_name)
            mat = Material()
            d = mat_data['diffuse']
            mat.diffuse = float32x3(float(d[0]), float(d[1]), float(d[2]))
            mat_idx = len(self._params.materials)
            self._materials[mat_name] = (mat, mat_idx)
            self._params.materials.append(mat)

    def load_lights(self, lights):
        def parse_f32x3(name, data):
            v = data[name]
            return float32x3(float(v[0]), float(v[1]), float(v[2]))

        for lgt_data in lights:
            lgt = Light()
            lgt.intensity = parse_f32x3('intensity', lgt_data)
            if lgt_data['type'].lower() == 'point':
                lgt.type = int32(0)
                lgt.position = parse_f32x3('position', lgt_data)
            elif lgt_data['type'].lower() == 'area':
                lgt.type = int32(1)
                shp_data = lgt_data['shape']
                if shp_data['type'].lower() == 'rectangle':
                    lgt.shp_type = int32(0)
                    rect = Rectangle()
                    rect.p = parse_f32x3('p', shp_data)
                    rect.edge_a = parse_f32x3('edge_a', shp_data)
                    rect.edge_b = parse_f32x3('edge_b', shp_data)
                    rect.normal = parse_f32x3('normal', shp_data)
                    rect.update_area()
                    mat, mat_idx = self._materials[shp_data['material']]
                    rect.mat_id = int32(mat_idx)
                    shape_id = len(self._params.light_rectangles)
                    self._params.light_rectangles.append(rect)
                    lgt.shp_id = int32(shape_id)
                else:
                    raise ValueError("Shape %s is not supported for area light." % shp_data['type'])

            self._params.lights.append(lgt)

    def _calc_bbox(self, shape):
        val = calculate_mesh_bbox(shape.verts)
        shape.bbox_p0 = float32x3(val[0], val[1], val[2])
        shape.bbox_p1 = float32x3(val[4], val[5], val[6])

    def load_shapes(self, shapes, dir_name):
        for shp_data in shapes:
            full_path = os.path.join(dir_name, shp_data['fname'])
            shapes, materials = load_obj_shapes(full_path)
            self.load_materials(materials)
            mat_dict = {mat_data['name']: mat_data for mat_data in materials}
            for name, mat_name, shp in shapes:
                if name in self._shapes:
                    raise ValueError("Shape %s allready exist!" % name)
                self._shapes[name] = shp
                if mat_name is None:
                    mat_name = shp_data['material']
                if mat_name not in self._materials:
                    raise ValueError("Missing %s material for %s shape!" % (mat_name, name))
                mat, mat_idx = self._materials[mat_name]
                shp.mat_id = int32(mat_idx)
                self._calc_bbox(shp)
                shp.nodes = create_bvh_tree_nodes(shp)
                shp.nodes16 = create_bvh16_tree_nodes(shp)
                # Mesh as area light
                if mat_name in mat_dict and 'emission' in mat_dict[mat_name]:
                    mesh_id = len(self._params.light_meshes)
                    self._params.light_meshes.append(shp)
                    lgt = Light()
                    e = mat_dict[mat_name]['emission']
                    lgt.intensity = float32x3(float(e[0]), float(e[1]), float(e[2]))
                    lgt.type = int32(1)  # area light
                    lgt.shp_type = int32(1)  # mesh
                    lgt.shp_id = int32(mesh_id)
                    self._params.lights.append(lgt)
                else:
                    self._params.shapes.append(shp)

    @staticmethod
    def parse_json(fname):
        ren = Renderer()
        with open(fname) as fobj:
            data = json.load(fobj)
            if 'global' in data:
                ren.set_global_params(data['global'], os.path.dirname(fobj.name))
            if 'camera' in data:
                ren.set_camera_params(data['camera'])
            if 'materials' in data:
                ren.load_materials(data['materials'])
            if 'lights' in data:
                ren.load_lights(data['lights'])
            if 'shapes' in data:
                ren.load_shapes(data['shapes'], os.path.dirname(fobj.name))
        return ren
