import os.path
from array import array
from simdy import array_float32x3, array_float32x2, array_int32, int32
from .params import Shape


def _extract_mat_data(data):
    mat_data = {}
    if 'Kd' in data:
        v = data['Kd']
        mat_data['diffuse'] = (float(v[0]), float(v[1]), float(v[2]))
    if 'Ke' in data:
        v = data['Ke']
        if v[0] != '0' or v[1] != '0' or v[2] != '0':
            mat_data['emission'] = (float(v[0]), float(v[1]), float(v[2]))
    return mat_data


def parse_matlib(fname):

    values = {}
    name = None
    all_materials = []

    fobj = open(fname)
    for line in fobj:
        line = line.strip()
        if line == "" or line[0] == '#': #skip blank lines and comments
            continue
        vals = line.split()
        if vals[0].strip() == 'newmtl':
            if name is not None:
                mat_data = _extract_mat_data(values)
                mat_data['name'] = name
                all_materials.append(mat_data)
            values.clear()
            name = vals[1].strip()
            continue

        values[vals[0].strip()] = vals[1:]

    if name is not None:
        mat_data = _extract_mat_data(values)
        mat_data['name'] = name
        all_materials.append(mat_data)

    return all_materials

# index starts at 1
# negative indexes also supported -1 last element

def _add_faces(words, mesh, verts, normals, uvs):
    def add_face_num(gl_arr, gl_idx, m_arr, m_d, m_faces, is_uv):
        if gl_idx > 0: # index in obj file starts at 1
            gl_idx = gl_idx - 1
        else:
            gl_idx = len(gl_arr) // 3 - abs(gl_idx)

        if gl_idx in m_d:
            m_faces.append(m_d[gl_idx])
        else:
            if is_uv:
                arr_idx = gl_idx * 2
                arr_new_idx = len(m_arr) // 2
                m_arr.append(gl_arr[arr_idx])
                m_arr.append(gl_arr[arr_idx + 1])
                m_d[gl_idx] = arr_new_idx
                m_faces.append(arr_new_idx)
            else:
                arr_idx = gl_idx * 3
                arr_new_idx = len(m_arr) // 3
                m_arr.append(gl_arr[arr_idx])
                m_arr.append(gl_arr[arr_idx + 1])
                m_arr.append(gl_arr[arr_idx + 2])
                m_d[gl_idx] = arr_new_idx
                m_faces.append(arr_new_idx)

    m_faces, (m_verts, m_verts_d), (m_normals, m_normlas_d), (m_uvs, m_uvs_d) = mesh
    face = []
    texco = []
    norms = []
    for v in words[1:]:
        w = v.split('/')
        face.append(int(w[0]))
        if len(w) > 1 and w[1] != "":
            texco.append(int(w[1]))
        if len(w) > 2:
            norms.append(int(w[2]))

    # split face on triangles
    for idx in range(len(face[2:])):
        add_face_num(verts, face[0], m_verts, m_verts_d, m_faces, False)
        add_face_num(verts, face[idx + 1], m_verts, m_verts_d, m_faces, False)
        add_face_num(verts, face[idx + 2], m_verts, m_verts_d, m_faces, False)
        if len(norms) > 0:
            add_face_num(normals, norms[0], m_normals, m_normlas_d, m_faces, False)
            add_face_num(normals, norms[idx + 1], m_normals, m_normlas_d, m_faces, False)
            add_face_num(normals, norms[idx + 2], m_normals, m_normlas_d, m_faces, False)
        else:
            m_faces.append(-1)
            m_faces.append(-1)
            m_faces.append(-1)
        if len(texco) > 0:
            add_face_num(uvs, texco[0], m_uvs, m_uvs_d, m_faces, True)
            add_face_num(uvs, texco[idx + 1], m_uvs, m_uvs_d, m_faces, True)
            add_face_num(uvs, texco[idx + 2], m_uvs, m_uvs_d, m_faces, True)
        else:
            m_faces.append(-1)
            m_faces.append(-1)
            m_faces.append(-1)


def load_raw_obj(fname):
    # group, material dict
    meshes = {}
    verts = array('f')
    uvs = array('f')
    normals = array('f')
    group = 'default'
    material = None
    all_materials = []

    with open(fname, 'r') as f:
        for line in f:
            line = line.strip()
            if line == "":
                continue  # skip blank lines
            words = line.split()
            if words[0][0] == "#":
                continue  # skip comments

            if words[0] == "v":
                verts.append(float(words[1]))
                verts.append(float(words[2]))
                verts.append(float(words[3]))
            elif words[0] == "vn":
                normals.append(float(words[1]))
                normals.append(float(words[2]))
                normals.append(float(words[3]))
            elif words[0] == "vt":
                uvs.append(float(words[1]))
                uvs.append(float(words[2]))
            elif words[0] == "g":
                group = '_'.join(words[1:]) if len(words) > 1 else 'default'
            elif words[0] == "usemtl":
                material = words[1]
            elif words[0] == "mtllib":
                mat_path = os.path.join(os.path.dirname(fname), words[1])
                all_materials = parse_matlib(mat_path)
            elif words[0] == 'f':  # indexes in previos arrays
                key = (group, material)
                if key not in meshes:
                    meshes[key] = array('i'), (array('f'), {}), (array('f'), {}), (array('f'), {})
                _add_faces(words, meshes[key], verts, normals, uvs)

    new_meshes = {}
    for key, value in meshes.items():
        m_faces, (m_verts, m_verts_d), (m_normals, m_normlas_d), (m_uvs, m_uvs_d) = value
        new_meshes[key] = (m_verts, m_normals, m_uvs, m_faces)
    return new_meshes, all_materials


def load_obj_shapes(full_path):
    meshes, materials = load_raw_obj(full_path)
    shapes = []
    for (name, mat_name), value in meshes.items():
        vertices, normals, uvs, faces = value

        shp = Shape()
        shp.verts = array_float32x3.from_py_array(vertices)
        shp.normals = array_float32x3.from_py_array(normals)
        shp.uvs = array_float32x2.from_py_array(uvs)
        shp.faces = array_int32.from_py_array(faces)
        shp.ntriangles = int32(len(shp.faces) // 9)

        shapes.append((name, mat_name, shp))
    return shapes, materials
