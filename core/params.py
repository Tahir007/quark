
from math import sqrt
from simdy import struct, int32, float32x3, array_float32x4, float32, array_float32x3,\
    array_float32x2, register_user_type, array, array_int32, int32x16, float32x16


class BvhNode(struct):
    __slots__ = ['bbox_p0', 'bbox_p1', 'n_prims', 'start', 'right_offset']

    def __init__(self):
        self.bbox_p0 = float32x3()
        self.bbox_p1 = float32x3()
        self.n_prims = int32()
        self.start = int32()
        self.right_offset = int32()


register_user_type(BvhNode, factory=BvhNode)


class array_BvhNode(array):
    def __init__(self, size=None):
        super(array_BvhNode, self).__init__(BvhNode, size=size)


class BvhNode16(struct):
    __slots__ = ['bbox_p0x', 'bbox_p0y', 'bbox_p0z', 'bbox_p1x', 'bbox_p1y', 'bbox_p1z',
                 'children', 'triangles', 'is_leaf']

    def __init__(self):
        self.bbox_p0x = float32x16()
        self.bbox_p0y = float32x16()
        self.bbox_p0z = float32x16()
        self.bbox_p1x = float32x16()
        self.bbox_p1y = float32x16()
        self.bbox_p1z = float32x16()
        self.children = int32x16()
        self.triangles = int32x16()
        self.is_leaf = int32x16()


register_user_type(BvhNode16, factory=BvhNode16)


class array_BvhNode16(array):
    def __init__(self, size=None):
        super(array_BvhNode16, self).__init__(BvhNode16, size=size)



class Shape(struct):
    __slots__ = ['verts', 'normals', 'uvs', 'faces', 'ntriangles',
                 'mat_id', 'bbox_p0', 'bbox_p1', 'nodes', 'nodes16']

    def __init__(self):
        self.verts = array_float32x3()
        self.normals = array_float32x3()
        self.uvs = array_float32x2()
        self.faces = array_int32()
        self.ntriangles = int32()
        self.mat_id = int32()
        self.bbox_p0 = float32x3()
        self.bbox_p1 = float32x3()
        self.nodes = array_BvhNode()
        self.nodes16 = array_BvhNode16()


register_user_type(Shape, factory=Shape)


class array_Shape(array):
    def __init__(self):
        super(array_Shape, self).__init__(Shape)


class Material(struct):
    __slots__ = ['diffuse']

    def __init__(self):
        self.diffuse = float32x3()


register_user_type(Material, factory=Material)


class array_Material(array):
    def __init__(self):
        super(array_Material, self).__init__(Material)


class Light(struct):
    __slots__ = ['type', 'intensity', 'position', 'shp_type', 'shp_id']

    def __init__(self):
        self.type = int32()
        self.intensity = float32x3()
        self.position = float32x3()

        # for area lights
        self.shp_type = int32()
        self.shp_id = int32()


register_user_type(Light, factory=Light)


class array_Light(array):
    def __init__(self):
        super(array_Light, self).__init__(Light)


class Ray(struct):
    __slots__ = ['origin', 'direction']

    def __init__(self):
        self.origin = float32x3()
        self.direction = float32x3()


register_user_type(Ray, factory=Ray)


class HitPoint(struct):
    __slots__ = ['normal', 'hit', 'wo', 'wi', 't', 'mat_id', 'light_id', 'light_tri_idx']

    def __init__(self):
        self.normal = float32x3()
        self.hit = float32x3()
        self.wo = float32x3()
        self.wi = float32x3()
        self.t = float32()
        self.mat_id = int32()
        self.light_id = int32()
        self.light_tri_idx = int32()


register_user_type(HitPoint, factory=HitPoint)


class BsdfSample(struct):
    __slots__ = ['factor', 'wi', 'pdf']

    def __init__(self):
        self.factor = float32x3()
        self.wi = float32x3()
        self.pdf = float32()


register_user_type(BsdfSample, factory=BsdfSample)


class LightSample(struct):
    __slots__ = ['intensity', 'wi', 'distance', 'pdf', 'delta_light']

    def __init__(self):
        self.intensity = float32x3()
        self.wi = float32x3()
        self.distance = float32()
        self.pdf = float32()
        self.delta_light = int32()


register_user_type(LightSample, factory=LightSample)


class TriangleData(struct):
    __slots__ = ['u', 'v', 't', 'index']

    def __init__(self):
        self.u = float32()
        self.v = float32()
        self.t = float32()
        self.index = int32()


register_user_type(TriangleData, factory=TriangleData)


class AreaSample(struct):
    __slots__ = ['position', 'normal', 'pdf_a']

    def __init__(self):
        self.position = float32x3()
        self.normal = float32x3()
        self.pdf_a = float32()


register_user_type(AreaSample, factory=AreaSample)



class Rectangle(struct):
    __slots__ = ['p', 'edge_a', 'edge_b', 'normal', 'inv_area', 'mat_id']

    def __init__(self):
        self.p = float32x3()
        self.edge_a = float32x3()
        self.edge_b = float32x3()
        self.normal = float32x3()
        self.inv_area = float32()
        self.mat_id = int32()

    def update_area(self):
        e1 = self.edge_a
        e1_sqr = e1[0] * e1[0] + e1[1] * e1[1] + e1[2] * e1[2]
        e2 = self.edge_b
        e2_sqr = e2[0] * e2[0] + e2[1] * e2[1] + e2[2] * e2[2]
        area = sqrt(e1_sqr) * sqrt(e2_sqr)
        inv_area = 1.0 / area
        self.inv_area = float32(inv_area)


register_user_type(Rectangle, factory=Rectangle)


class array_Rectangle(array):
    def __init__(self):
        super(array_Rectangle, self).__init__(Rectangle)


class Params(struct):
    __slots__ = ['resx', 'resy', 'color_buffer', 'tile_width', 'tile_height', 'cam_eye',
                 'cam_u', 'cam_v', 'cam_w', 'cam_fov', 'spp', 'flt_type', 'flt_radius',
                 'shapes', 'materials', 'lights', 'light_rectangles', 'light_meshes']

    def __init__(self):
        self.resx = int32()
        self.resy = int32()
        self.color_buffer = array_float32x4()
        self.tile_width = int32()
        self.tile_height = int32()
        self.cam_eye = float32x3()
        self.cam_u = float32x3()
        self.cam_v = float32x3()
        self.cam_w = float32x3()
        self.cam_fov = float32()
        self.spp = int32()
        self.flt_type = int32()
        self.flt_radius = float32()
        self.shapes = array_Shape()
        self.materials = array_Material()
        self.lights = array_Light()
        self.light_rectangles = array_Rectangle()
        self.light_meshes = array_Shape()


