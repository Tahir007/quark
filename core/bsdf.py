from simdy import simdy_kernel, float32x3, struct, register_user_type, float32
from .params import HitPoint, Params, BsdfSample


class Frame(struct):
    __slots__ = ['u', 'v', 'w']

    def __init__(self):
        self.u = float32x3(1, 0, 0)
        self.v = float32x3(0, 1, 0)
        self.w = float32x3(0, 0, 1)


register_user_type(Frame, factory=Frame)


@simdy_kernel
def bsdf(hp: HitPoint, params: Params):
    # INV_PI = 0.3183098861837
    # brdf * cos_theta
    mat = params.materials[hp.mat_id]
    return mat.diffuse * 0.3183098861837 * abs(dot(hp.normal, hp.wi))


@simdy_kernel
def construct_frame(n: float32x3, frame: Frame):
    # if n[2] < float32(-0.9999999):
    #     frame.u = float32x3(0.0, -1.0, 0.0)
    #     frame.v = float32x3(-1.0, 0.0, 0.0)
    #     frame.w = n
    # else:
    #     a = float32(1.0) / (float32(1.0) + n[2])
    #     neg_vector = n * float32x3(-1.0)
    #     b = neg_vector[0] * n[1] * a
    #     frame.u = float32x3(float32(1.0) - n[0] * n[0] * a, b, neg_vector[0])
    #     frame.v = float32x3(b, float32(1.0) - n[1] * n[1] * a, neg_vector[1])
    #     frame.w = n

    if n[2] < float32(0.0):
        a = float32(1.0) / (float32(1.0) - n[2])
        b = n[0] * n[1] * a
        frame.u = float32x3(float32(1.0) - n[0] * n[0] * a, b * -1.0, n[0])
        frame.v = float32x3(b, n[1] * n[1] * a - 1.0, n[1] * -1.0)
        frame.w = n
    else:
        a = float32(1.0) / (float32(1.0) + n[2])
        neg_vector = n * float32x3(-1.0)
        b = neg_vector[0] * n[1] * a
        frame.u = float32x3(float32(1.0) - n[0] * n[0] * a, b, neg_vector[0])
        frame.v = float32x3(b, float32(1.0) - n[1] * n[1] * a, neg_vector[1])
        frame.w = n


@simdy_kernel
def sample_cosine_hemi(normal: float32x3):
    r1 = random_float32()
    r2 = random_float32()
    tmp = sqrt(1.0 - r1)
    frame = Frame()
    construct_frame(normal, frame)
    wi = frame.u * (tmp * cos(6.28318530717958 * r2)) + frame.v * (tmp * sin(6.28318530717958 * r2)) + frame.w * sqrt(r1)
    return wi / float32x3(sqrt(dot(wi, wi)))  # normalization of vector


@simdy_kernel
def sample_bsdf(hp: HitPoint, params: Params, bs: BsdfSample):
    # Sampling brdf * cos_theta
    mat = params.materials[hp.mat_id]
    wi = sample_cosine_hemi(hp.normal)
    bs.wi = wi
    bs.pdf = dot(hp.normal, wi) * 0.3183098861837
    bs.factor = mat.diffuse
    return 1


@simdy_kernel
def pdf_bsdf(hp: HitPoint, params: Params):
    # Pdf of brdf * cos_theta
    mat = params.materials[hp.mat_id]
    pdf = dot(hp.normal, hp.wi) * 0.3183098861837
    return pdf