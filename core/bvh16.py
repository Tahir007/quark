
from simdy import simdy_kernel, int32, struct, register_user_type, float32x3, float32,\
    float32x2, int32x2, array, float32x4, int32x4, float32x8, int32x8, float32x16, int32x16
from .params import array_BvhNode16, Shape, TriangleData, Ray
from .core import isect_bool_shape_triangle, isect_shape_triangle


class BvhNode2(struct):
    __slots__ = ['bbox_p0x', 'bbox_p0y', 'bbox_p0z', 'bbox_p1x', 'bbox_p1y', 'bbox_p1z',
                 'children', 'triangles', 'is_leaf']

    def __init__(self):
        self.bbox_p0x = float32x2()
        self.bbox_p0y = float32x2()
        self.bbox_p0z = float32x2()
        self.bbox_p1x = float32x2()
        self.bbox_p1y = float32x2()
        self.bbox_p1z = float32x2()
        self.children = int32x2()
        self.triangles = int32x2()
        self.is_leaf = int32x2()


register_user_type(BvhNode2, factory=BvhNode2)


class array_BvhNode2(array):
    def __init__(self, size=None):
        super(array_BvhNode2, self).__init__(BvhNode2, size=size)


class BvhNode4(struct):
    __slots__ = ['bbox_p0x', 'bbox_p0y', 'bbox_p0z', 'bbox_p1x', 'bbox_p1y', 'bbox_p1z',
                 'children', 'triangles', 'is_leaf']

    def __init__(self):
        self.bbox_p0x = float32x4()
        self.bbox_p0y = float32x4()
        self.bbox_p0z = float32x4()
        self.bbox_p1x = float32x4()
        self.bbox_p1y = float32x4()
        self.bbox_p1z = float32x4()
        self.children = int32x4()
        self.triangles = int32x4()
        self.is_leaf = int32x4()


register_user_type(BvhNode4, factory=BvhNode4)


class array_BvhNode4(array):
    def __init__(self, size=None):
        super(array_BvhNode4, self).__init__(BvhNode4, size=size)


@simdy_kernel
def _create_bvh2_nodes(shape: Shape, nodes: array_BvhNode2) -> int32:

    bvh_nodes = shape.nodes

    bvh_node = bvh_nodes[0]
    if bvh_node.right_offset == 0:  # root node is leaf
        bvh2_node = nodes[0]
        bvh2_node.bbox_p0x = float32x2(bvh_node.bbox_p0[0])
        bvh2_node.bbox_p0y = float32x2(bvh_node.bbox_p0[1])
        bvh2_node.bbox_p0z = float32x2(bvh_node.bbox_p0[2])
        bvh2_node.bbox_p1x = float32x2(bvh_node.bbox_p1[0])
        bvh2_node.bbox_p1y = float32x2(bvh_node.bbox_p1[1])
        bvh2_node.bbox_p1z = float32x2(bvh_node.bbox_p1[2])
        bvh2_node.triangles = int32x2(bvh_node.start, -1)
        bvh2_node.is_leaf = int32x2(1)
        bvh2_node.children = int32x2(-1)
        return 1

    stack_bvh2 = array(int32, 128)
    stack_bvh = array(int32, 128)
    stack_ptr = 0

    stack_bvh2[stack_ptr] = 0
    stack_bvh[stack_ptr] = 0
    stack_ptr += 1
    cur_bvh2_node = 1

    while stack_ptr > 0:
        stack_ptr -= 1
        bvh_node_idx = stack_bvh[stack_ptr]
        bvh2_node_idx = stack_bvh2[stack_ptr]

        bvh_node = bvh_nodes[bvh_node_idx]
        bvh_node_l = bvh_nodes[bvh_node_idx + 1]
        bvh_node_r = bvh_nodes[bvh_node_idx + bvh_node.right_offset]
        bvh2_node = nodes[bvh2_node_idx]
        bvh2_node.bbox_p0x = float32x2(bvh_node_l.bbox_p0[0], bvh_node_r.bbox_p0[0])
        bvh2_node.bbox_p0y = float32x2(bvh_node_l.bbox_p0[1], bvh_node_r.bbox_p0[1])
        bvh2_node.bbox_p0z = float32x2(bvh_node_l.bbox_p0[2], bvh_node_r.bbox_p0[2])
        bvh2_node.bbox_p1x = float32x2(bvh_node_l.bbox_p1[0], bvh_node_r.bbox_p1[0])
        bvh2_node.bbox_p1y = float32x2(bvh_node_l.bbox_p1[1], bvh_node_r.bbox_p1[1])
        bvh2_node.bbox_p1z = float32x2(bvh_node_l.bbox_p1[2], bvh_node_r.bbox_p1[2])
        is_leaf_l = 0
        tri_l = -1
        child_l = -1
        if bvh_node_l.right_offset == 0:  # node is leaf
            is_leaf_l = 1
            tri_l = bvh_node_l.start
        else:
            child_l = cur_bvh2_node
            stack_bvh2[stack_ptr] = cur_bvh2_node
            stack_bvh[stack_ptr] = bvh_node_idx + 1
            stack_ptr += 1
            cur_bvh2_node += 1

        is_leaf_r = 0
        tri_r = -1
        child_r = -1
        if bvh_node_r.right_offset == 0:  # node is leaf
            is_leaf_r = 1
            tri_r = bvh_node_r.start
        else:
            child_r = cur_bvh2_node
            stack_bvh2[stack_ptr] = cur_bvh2_node
            stack_bvh[stack_ptr] = bvh_node_idx + bvh_node.right_offset
            stack_ptr += 1
            cur_bvh2_node += 1

        bvh2_node.is_leaf = int32x2(is_leaf_l, is_leaf_r)
        bvh2_node.triangles = int32x2(tri_l, tri_r)
        bvh2_node.children = int32x2(child_l, child_r)

    return cur_bvh2_node


@simdy_kernel
def _create_bvh4_nodes(bvh2_nodes: array_BvhNode2, bvh4_nodes: array_BvhNode4) -> int32:

    bvh2_node = bvh2_nodes[0]

    if bvh2_node.is_leaf[0] == 1 and bvh2_node.is_leaf[1] == 1:  # root node is leaf
        bvh4_node = bvh4_nodes[0]
        bvh4_node.bbox_p0x = float32x4(bvh2_node.bbox_p0x, bvh2_node.bbox_p0x)
        bvh4_node.bbox_p0y = float32x4(bvh2_node.bbox_p0y, bvh2_node.bbox_p0y)
        bvh4_node.bbox_p0z = float32x4(bvh2_node.bbox_p0z, bvh2_node.bbox_p0z)
        bvh4_node.bbox_p1x = float32x4(bvh2_node.bbox_p1x, bvh2_node.bbox_p1x)
        bvh4_node.bbox_p1y = float32x4(bvh2_node.bbox_p1y, bvh2_node.bbox_p1y)
        bvh4_node.bbox_p1z = float32x4(bvh2_node.bbox_p1z, bvh2_node.bbox_p1z)

        bvh4_node.triangles = int32x4(bvh2_node.triangles, int32x2(-1))
        bvh4_node.is_leaf = int32x4(1)
        bvh4_node.children = int32x4(-1)
        return 1

    stack_bvh4 = array(int32, 128)
    stack_bvh2 = array(int32, 128)
    stack_ptr = 0

    stack_bvh4[stack_ptr] = 0
    stack_bvh2[stack_ptr] = 0
    stack_ptr += 1
    cur_bvh4_node = 1

    while stack_ptr > 0:
        stack_ptr -= 1
        bvh2_node_idx = stack_bvh2[stack_ptr]
        bvh4_node_idx = stack_bvh4[stack_ptr]

        bvh2_node = bvh2_nodes[bvh2_node_idx]

        if bvh2_node.is_leaf[0]:
            bvh2_l_bbox_p0x = float32x2(bvh2_node.bbox_p0x[0])
            bvh2_l_bbox_p0y = float32x2(bvh2_node.bbox_p0y[0])
            bvh2_l_bbox_p0z = float32x2(bvh2_node.bbox_p0z[0])
            bvh2_l_bbox_p1x = float32x2(bvh2_node.bbox_p1x[0])
            bvh2_l_bbox_p1y = float32x2(bvh2_node.bbox_p1y[0])
            bvh2_l_bbox_p1z = float32x2(bvh2_node.bbox_p1z[0])
            bvh2_l_triangles = int32x2(bvh2_node.triangles[0], -1)
            bvh2_l_children = int32x2(-1)
            bvh2_l_is_leaf = int32x2(1)
        else:
            bvh2_l_node = bvh2_nodes[bvh2_node.children[0]]
            bvh2_l_bbox_p0x = bvh2_l_node.bbox_p0x
            bvh2_l_bbox_p0y = bvh2_l_node.bbox_p0y
            bvh2_l_bbox_p0z = bvh2_l_node.bbox_p0z
            bvh2_l_bbox_p1x = bvh2_l_node.bbox_p1x
            bvh2_l_bbox_p1y = bvh2_l_node.bbox_p1y
            bvh2_l_bbox_p1z = bvh2_l_node.bbox_p1z
            bvh2_l_triangles = bvh2_l_node.triangles
            bvh2_l_is_leaf = bvh2_l_node.is_leaf

            if bvh2_l_node.is_leaf[0]:
                child_l = -1
            else:
                child_l = cur_bvh4_node
                stack_bvh4[stack_ptr] = cur_bvh4_node
                stack_bvh2[stack_ptr] = bvh2_l_node.children[0]
                stack_ptr += 1
                cur_bvh4_node += 1

            if bvh2_l_node.is_leaf[1]:
                child_r = -1
            else:
                child_r = cur_bvh4_node
                stack_bvh4[stack_ptr] = cur_bvh4_node
                stack_bvh2[stack_ptr] = bvh2_l_node.children[1]
                stack_ptr += 1
                cur_bvh4_node += 1

            bvh2_l_children = int32x2(child_l, child_r)

        if bvh2_node.is_leaf[1]:
            bvh2_r_bbox_p0x = float32x2(bvh2_node.bbox_p0x[1])
            bvh2_r_bbox_p0y = float32x2(bvh2_node.bbox_p0y[1])
            bvh2_r_bbox_p0z = float32x2(bvh2_node.bbox_p0z[1])
            bvh2_r_bbox_p1x = float32x2(bvh2_node.bbox_p1x[1])
            bvh2_r_bbox_p1y = float32x2(bvh2_node.bbox_p1y[1])
            bvh2_r_bbox_p1z = float32x2(bvh2_node.bbox_p1z[1])
            bvh2_r_triangles = int32x2(bvh2_node.triangles[1], -1)
            bvh2_r_children = int32x2(-1)
            bvh2_r_is_leaf = int32x2(1)
        else:
            bvh2_r_node = bvh2_nodes[bvh2_node.children[1]]
            bvh2_r_bbox_p0x = bvh2_r_node.bbox_p0x
            bvh2_r_bbox_p0y = bvh2_r_node.bbox_p0y
            bvh2_r_bbox_p0z = bvh2_r_node.bbox_p0z
            bvh2_r_bbox_p1x = bvh2_r_node.bbox_p1x
            bvh2_r_bbox_p1y = bvh2_r_node.bbox_p1y
            bvh2_r_bbox_p1z = bvh2_r_node.bbox_p1z
            bvh2_r_triangles = bvh2_r_node.triangles
            bvh2_r_is_leaf = bvh2_r_node.is_leaf

            if bvh2_r_node.is_leaf[0]:
                child_l = -1
            else:
                child_l = cur_bvh4_node
                stack_bvh4[stack_ptr] = cur_bvh4_node
                stack_bvh2[stack_ptr] = bvh2_r_node.children[0]
                stack_ptr += 1
                cur_bvh4_node += 1

            if bvh2_r_node.is_leaf[1]:
                child_r = -1
            else:
                child_r = cur_bvh4_node
                stack_bvh4[stack_ptr] = cur_bvh4_node
                stack_bvh2[stack_ptr] = bvh2_r_node.children[1]
                stack_ptr += 1
                cur_bvh4_node += 1

            bvh2_r_children = int32x2(child_l, child_r)

        bvh4_node = bvh4_nodes[bvh4_node_idx]
        bvh4_node.bbox_p0x = float32x4(bvh2_l_bbox_p0x, bvh2_r_bbox_p0x)
        bvh4_node.bbox_p0y = float32x4(bvh2_l_bbox_p0y, bvh2_r_bbox_p0y)
        bvh4_node.bbox_p0z = float32x4(bvh2_l_bbox_p0z, bvh2_r_bbox_p0z)
        bvh4_node.bbox_p1x = float32x4(bvh2_l_bbox_p1x, bvh2_r_bbox_p1x)
        bvh4_node.bbox_p1y = float32x4(bvh2_l_bbox_p1y, bvh2_r_bbox_p1y)
        bvh4_node.bbox_p1z = float32x4(bvh2_l_bbox_p1z, bvh2_r_bbox_p1z)

        bvh4_node.triangles = int32x4(bvh2_l_triangles, bvh2_r_triangles)
        bvh4_node.is_leaf = int32x4(bvh2_l_is_leaf, bvh2_r_is_leaf)
        bvh4_node.children = int32x4(bvh2_l_children, bvh2_r_children)

    return cur_bvh4_node


@simdy_kernel
def _create_bvh16_nodes(bvh4_nodes: array_BvhNode4, bvh16_nodes: array_BvhNode16) -> int32:

    bvh4_node = bvh4_nodes[0]

    if bvh4_node.is_leaf[0] == 1 and bvh4_node.is_leaf[1] == 1 and bvh4_node.is_leaf[2] == 1 and bvh4_node.is_leaf[3] == 1:
        bvh16_node = bvh16_nodes[0]
        bb = float32x8(bvh4_node.bbox_p0x, bvh4_node.bbox_p0x)
        bvh16_node.bbox_p0x = float32x16(bb, bb)
        bb = float32x8(bvh4_node.bbox_p0y, bvh4_node.bbox_p0y)
        bvh16_node.bbox_p0y = float32x16(bb, bb)
        bb = float32x8(bvh4_node.bbox_p0z, bvh4_node.bbox_p0z)
        bvh16_node.bbox_p0z = float32x16(bb, bb)
        bb = float32x8(bvh4_node.bbox_p1x, bvh4_node.bbox_p1x)
        bvh16_node.bbox_p1x = float32x16(bb, bb)
        bb = float32x8(bvh4_node.bbox_p1y, bvh4_node.bbox_p1y)
        bvh16_node.bbox_p1y = float32x16(bb, bb)
        bb = float32x8(bvh4_node.bbox_p1z, bvh4_node.bbox_p1z)
        bvh16_node.bbox_p1z = float32x16(bb, bb)

        bvh16_node.triangles = int32x16(int32x8(bvh4_node.triangles, int32x4(-1)), int32x8(-1))
        bvh16_node.is_leaf = int32x16(1)
        bvh16_node.children = int32x16(-1)
        return 1

    stack_bvh16 = array(int32, 128)
    stack_bvh4 = array(int32, 128)
    stack_ptr = 0

    stack_bvh16[stack_ptr] = 0
    stack_bvh4[stack_ptr] = 0
    stack_ptr += 1
    cur_bvh16_node = 1

    while stack_ptr > 0:
        stack_ptr -= 1
        bvh4_node_idx = stack_bvh4[stack_ptr]
        bvh16_node_idx = stack_bvh16[stack_ptr]

        bvh4_node = bvh4_nodes[bvh4_node_idx]

        if bvh4_node.is_leaf[0]:
            bvh4_l_bbox_p0x = float32x4(bvh4_node.bbox_p0x[0])
            bvh4_l_bbox_p0y = float32x4(bvh4_node.bbox_p0y[0])
            bvh4_l_bbox_p0z = float32x4(bvh4_node.bbox_p0z[0])
            bvh4_l_bbox_p1x = float32x4(bvh4_node.bbox_p1x[0])
            bvh4_l_bbox_p1y = float32x4(bvh4_node.bbox_p1y[0])
            bvh4_l_bbox_p1z = float32x4(bvh4_node.bbox_p1z[0])
            bvh4_l_triangles = int32x4(bvh4_node.triangles[0], -1, -1, -1)
            bvh4_l_children = int32x4(-1)
            bvh4_l_is_leaf = int32x4(1)
        else:
            bvh4_l_node = bvh4_nodes[bvh4_node.children[0]]
            bvh4_l_bbox_p0x = bvh4_l_node.bbox_p0x
            bvh4_l_bbox_p0y = bvh4_l_node.bbox_p0y
            bvh4_l_bbox_p0z = bvh4_l_node.bbox_p0z
            bvh4_l_bbox_p1x = bvh4_l_node.bbox_p1x
            bvh4_l_bbox_p1y = bvh4_l_node.bbox_p1y
            bvh4_l_bbox_p1z = bvh4_l_node.bbox_p1z
            bvh4_l_triangles = bvh4_l_node.triangles
            bvh4_l_is_leaf = bvh4_l_node.is_leaf

            if bvh4_l_node.is_leaf[0]:
                child_l = -1
            else:
                child_l = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[0]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[1]:
                child_lk = -1
            else:
                child_lk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[1]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[2]:
                child_r = -1
            else:
                child_r = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[2]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[3]:
                child_rk = -1
            else:
                child_rk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[3]
                stack_ptr += 1
                cur_bvh16_node += 1

            bvh4_l_children = int32x4(child_l, child_lk, child_r, child_rk)

        if bvh4_node.is_leaf[1]:
            bvh4_lk_bbox_p0x = float32x4(bvh4_node.bbox_p0x[1])
            bvh4_lk_bbox_p0y = float32x4(bvh4_node.bbox_p0y[1])
            bvh4_lk_bbox_p0z = float32x4(bvh4_node.bbox_p0z[1])
            bvh4_lk_bbox_p1x = float32x4(bvh4_node.bbox_p1x[1])
            bvh4_lk_bbox_p1y = float32x4(bvh4_node.bbox_p1y[1])
            bvh4_lk_bbox_p1z = float32x4(bvh4_node.bbox_p1z[1])
            bvh4_lk_triangles = int32x4(bvh4_node.triangles[1], -1, -1, -1)
            bvh4_lk_children = int32x4(-1)
            bvh4_lk_is_leaf = int32x4(1)
        else:
            bvh4_l_node = bvh4_nodes[bvh4_node.children[1]]
            bvh4_lk_bbox_p0x = bvh4_l_node.bbox_p0x
            bvh4_lk_bbox_p0y = bvh4_l_node.bbox_p0y
            bvh4_lk_bbox_p0z = bvh4_l_node.bbox_p0z
            bvh4_lk_bbox_p1x = bvh4_l_node.bbox_p1x
            bvh4_lk_bbox_p1y = bvh4_l_node.bbox_p1y
            bvh4_lk_bbox_p1z = bvh4_l_node.bbox_p1z
            bvh4_lk_triangles = bvh4_l_node.triangles
            bvh4_lk_is_leaf = bvh4_l_node.is_leaf

            if bvh4_l_node.is_leaf[0]:
                child_l = -1
            else:
                child_l = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[0]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[1]:
                child_lk = -1
            else:
                child_lk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[1]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[2]:
                child_r = -1
            else:
                child_r = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[2]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[3]:
                child_rk = -1
            else:
                child_rk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[3]
                stack_ptr += 1
                cur_bvh16_node += 1

            bvh4_lk_children = int32x4(child_l, child_lk, child_r, child_rk)

        if bvh4_node.is_leaf[2]:
            bvh4_r_bbox_p0x = float32x4(bvh4_node.bbox_p0x[2])
            bvh4_r_bbox_p0y = float32x4(bvh4_node.bbox_p0y[2])
            bvh4_r_bbox_p0z = float32x4(bvh4_node.bbox_p0z[2])
            bvh4_r_bbox_p1x = float32x4(bvh4_node.bbox_p1x[2])
            bvh4_r_bbox_p1y = float32x4(bvh4_node.bbox_p1y[2])
            bvh4_r_bbox_p1z = float32x4(bvh4_node.bbox_p1z[2])
            bvh4_r_triangles = int32x4(bvh4_node.triangles[2], -1, -1, -1)
            bvh4_r_children = int32x4(-1)
            bvh4_r_is_leaf = int32x4(1)
        else:
            bvh4_l_node = bvh4_nodes[bvh4_node.children[2]]
            bvh4_r_bbox_p0x = bvh4_l_node.bbox_p0x
            bvh4_r_bbox_p0y = bvh4_l_node.bbox_p0y
            bvh4_r_bbox_p0z = bvh4_l_node.bbox_p0z
            bvh4_r_bbox_p1x = bvh4_l_node.bbox_p1x
            bvh4_r_bbox_p1y = bvh4_l_node.bbox_p1y
            bvh4_r_bbox_p1z = bvh4_l_node.bbox_p1z
            bvh4_r_triangles = bvh4_l_node.triangles
            bvh4_r_is_leaf = bvh4_l_node.is_leaf

            if bvh4_l_node.is_leaf[0]:
                child_l = -1
            else:
                child_l = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[0]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[1]:
                child_lk = -1
            else:
                child_lk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[1]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[2]:
                child_r = -1
            else:
                child_r = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[2]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[3]:
                child_rk = -1
            else:
                child_rk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[3]
                stack_ptr += 1
                cur_bvh16_node += 1

            bvh4_r_children = int32x4(child_l, child_lk, child_r, child_rk)

        if bvh4_node.is_leaf[3]:
            bvh4_rk_bbox_p0x = float32x4(bvh4_node.bbox_p0x[3])
            bvh4_rk_bbox_p0y = float32x4(bvh4_node.bbox_p0y[3])
            bvh4_rk_bbox_p0z = float32x4(bvh4_node.bbox_p0z[3])
            bvh4_rk_bbox_p1x = float32x4(bvh4_node.bbox_p1x[3])
            bvh4_rk_bbox_p1y = float32x4(bvh4_node.bbox_p1y[3])
            bvh4_rk_bbox_p1z = float32x4(bvh4_node.bbox_p1z[3])
            bvh4_rk_triangles = int32x4(bvh4_node.triangles[3], -1, -1, -1)
            bvh4_rk_children = int32x4(-1)
            bvh4_rk_is_leaf = int32x4(1)
        else:
            bvh4_l_node = bvh4_nodes[bvh4_node.children[3]]
            bvh4_rk_bbox_p0x = bvh4_l_node.bbox_p0x
            bvh4_rk_bbox_p0y = bvh4_l_node.bbox_p0y
            bvh4_rk_bbox_p0z = bvh4_l_node.bbox_p0z
            bvh4_rk_bbox_p1x = bvh4_l_node.bbox_p1x
            bvh4_rk_bbox_p1y = bvh4_l_node.bbox_p1y
            bvh4_rk_bbox_p1z = bvh4_l_node.bbox_p1z
            bvh4_rk_triangles = bvh4_l_node.triangles
            bvh4_rk_is_leaf = bvh4_l_node.is_leaf

            if bvh4_l_node.is_leaf[0]:
                child_l = -1
            else:
                child_l = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[0]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[1]:
                child_lk = -1
            else:
                child_lk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[1]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[2]:
                child_r = -1
            else:
                child_r = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[2]
                stack_ptr += 1
                cur_bvh16_node += 1

            if bvh4_l_node.is_leaf[3]:
                child_rk = -1
            else:
                child_rk = cur_bvh16_node
                stack_bvh16[stack_ptr] = cur_bvh16_node
                stack_bvh4[stack_ptr] = bvh4_l_node.children[3]
                stack_ptr += 1
                cur_bvh16_node += 1

            bvh4_rk_children = int32x4(child_l, child_lk, child_r, child_rk)


        bvh16_node = bvh16_nodes[bvh16_node_idx]
        bvh16_node.bbox_p0x = float32x16(float32x8(bvh4_l_bbox_p0x, bvh4_lk_bbox_p0x), float32x8(bvh4_r_bbox_p0x, bvh4_rk_bbox_p0x))
        bvh16_node.bbox_p0y = float32x16(float32x8(bvh4_l_bbox_p0y, bvh4_lk_bbox_p0y), float32x8(bvh4_r_bbox_p0y, bvh4_rk_bbox_p0y))
        bvh16_node.bbox_p0z = float32x16(float32x8(bvh4_l_bbox_p0z, bvh4_lk_bbox_p0z), float32x8(bvh4_r_bbox_p0z, bvh4_rk_bbox_p0z))
        bvh16_node.bbox_p1x = float32x16(float32x8(bvh4_l_bbox_p1x, bvh4_lk_bbox_p1x), float32x8(bvh4_r_bbox_p1x, bvh4_rk_bbox_p1x))
        bvh16_node.bbox_p1y = float32x16(float32x8(bvh4_l_bbox_p1y, bvh4_lk_bbox_p1y), float32x8(bvh4_r_bbox_p1y, bvh4_rk_bbox_p1y))
        bvh16_node.bbox_p1z = float32x16(float32x8(bvh4_l_bbox_p1z, bvh4_lk_bbox_p1z), float32x8(bvh4_r_bbox_p1z, bvh4_rk_bbox_p1z))

        bvh16_node.triangles = int32x16(int32x8(bvh4_l_triangles, bvh4_lk_triangles), int32x8(bvh4_r_triangles, bvh4_rk_triangles))
        bvh16_node.children = int32x16(int32x8(bvh4_l_children, bvh4_lk_children), int32x8(bvh4_r_children, bvh4_rk_children))
        bvh16_node.is_leaf = int32x16(int32x8(bvh4_l_is_leaf, bvh4_lk_is_leaf), int32x8(bvh4_r_is_leaf, bvh4_rk_is_leaf))

    return cur_bvh16_node


@simdy_kernel
def _copy_bvh16_nodes(in_nodes: array_BvhNode16, out_nodes: array_BvhNode16, n: int32):
    for i in range(n):
        in_node = in_nodes[i]

        bbox_p0x = in_node.bbox_p0x
        bbox_p0y = in_node.bbox_p0y
        bbox_p0z = in_node.bbox_p0z
        bbox_p1x = in_node.bbox_p1x
        bbox_p1y = in_node.bbox_p1y
        bbox_p1z = in_node.bbox_p1z
        children = in_node.children
        triangles = in_node.triangles
        is_leaf = in_node.is_leaf

        out_node = out_nodes[i]
        out_node.bbox_p0x = bbox_p0x
        out_node.bbox_p0y = bbox_p0y
        out_node.bbox_p0z = bbox_p0z
        out_node.bbox_p1x = bbox_p1x
        out_node.bbox_p1y = bbox_p1y
        out_node.bbox_p1z = bbox_p1z
        out_node.children = children
        out_node.triangles = triangles
        out_node.is_leaf = is_leaf


def create_bvh16_tree_nodes(shape):

    bvh2_nodes = array_BvhNode2(size=shape.ntriangles)
    n_bvh2_nodes = _create_bvh2_nodes(shape, bvh2_nodes)

    bvh4_nodes = array_BvhNode4(size=shape.ntriangles)
    n_bvh4_nodes = _create_bvh4_nodes(bvh2_nodes, bvh4_nodes)

    tmp_bvh16_nodes = array_BvhNode16(size=n_bvh4_nodes)
    n_bvh16_nodes = _create_bvh16_nodes(bvh4_nodes, tmp_bvh16_nodes)

    bvh16_nodes = array_BvhNode16(size=n_bvh16_nodes)
    _copy_bvh16_nodes(tmp_bvh16_nodes, bvh16_nodes, n_bvh16_nodes)
    return bvh16_nodes


class Bvh16Traversal(struct):
    __slots__ = ['node_idx', 'min_t']

    def __init__(self):
        self.node_idx = int32()
        self.min_t = float32()


register_user_type(Bvh16Traversal, factory=Bvh16Traversal)


@simdy_kernel
def isect_mesh_bvh16(ray: Ray, shape: Shape, max_dist: float32, tdata: TriangleData):

    isec_dist = max_dist
    hit_ocur = 0
    bvh16_nodes = shape.nodes16

    origin = ray.origin
    inv_ray_dir = float32x3(1.0) / ray.direction
    rx = float32x16(inv_ray_dir[0])
    ry = float32x16(inv_ray_dir[1])
    rz = float32x16(inv_ray_dir[2])
    ox = float32x16(origin[0])
    oy = float32x16(origin[1])
    oz = float32x16(origin[2])

    traversal_stack = array(Bvh16Traversal, 64)
    stack_ptr = 0

    # push root node on the stack
    obj = traversal_stack[stack_ptr]
    obj.node_idx = 0
    obj.min_t = float32(-1e30)

    while stack_ptr >= 0:
        obj = traversal_stack[stack_ptr]
        n_idx = obj.node_idx
        near = obj.min_t
        stack_ptr -= 1

        if near > isec_dist:
            continue

        bvh16_node = bvh16_nodes[n_idx]
        t1 = (bvh16_node.bbox_p0x - ox) * rx
        t2 = (bvh16_node.bbox_p1x - ox) * rx
        tmin = min(t1, t2)
        tmax = max(t1, t2)

        t1 = (bvh16_node.bbox_p0y - oy) * ry
        t2 = (bvh16_node.bbox_p1y - oy) * ry

        tmin = max(tmin, min(t1, t2))
        tmax = min(tmax, max(t1, t2))

        t1 = (bvh16_node.bbox_p0z - oz) * rz
        t2 = (bvh16_node.bbox_p1z - oz) * rz

        tmin = max(tmin, min(t1, t2))
        tmax = min(tmax, max(t1, t2))

        result = select(int32x16(1), int32x16(0), tmax > max(tmin, float32x16(0)))
        hit_min_t = max(tmin, float32x16(0))

        for i in range(16):
            if result[i] == 1:  # ray intersect bbox
                if bvh16_node.is_leaf[i]:
                    if bvh16_node.triangles[i] >= 0:
                        if isect_shape_triangle(ray, shape, isec_dist, bvh16_node.triangles[i], tdata):
                            isec_dist = tdata.t
                            hit_ocur = 1
                else:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = bvh16_node.children[i]
                    obj.min_t = hit_min_t[i]

    return hit_ocur


@simdy_kernel
def isect_bool_mesh_bvh16(ray: Ray, shape: Shape, max_dist: float32):

    isec_dist = max_dist
    bvh16_nodes = shape.nodes16

    origin = ray.origin
    inv_ray_dir = float32x3(1.0) / ray.direction
    rx = float32x16(inv_ray_dir[0])
    ry = float32x16(inv_ray_dir[1])
    rz = float32x16(inv_ray_dir[2])
    ox = float32x16(origin[0])
    oy = float32x16(origin[1])
    oz = float32x16(origin[2])

    traversal_stack = array(Bvh16Traversal, 64)
    stack_ptr = 0

    # push root node on the stack
    obj = traversal_stack[stack_ptr]
    obj.node_idx = 0
    obj.min_t = float32(-1e30)

    while stack_ptr >= 0:
        obj = traversal_stack[stack_ptr]
        n_idx = obj.node_idx
        near = obj.min_t
        stack_ptr -= 1

        if near > isec_dist:
            continue

        bvh16_node = bvh16_nodes[n_idx]
        t1 = (bvh16_node.bbox_p0x - ox) * rx
        t2 = (bvh16_node.bbox_p1x - ox) * rx
        tmin = min(t1, t2)
        tmax = max(t1, t2)

        t1 = (bvh16_node.bbox_p0y - oy) * ry
        t2 = (bvh16_node.bbox_p1y - oy) * ry

        tmin = max(tmin, min(t1, t2))
        tmax = min(tmax, max(t1, t2))

        t1 = (bvh16_node.bbox_p0z - oz) * rz
        t2 = (bvh16_node.bbox_p1z - oz) * rz

        tmin = max(tmin, min(t1, t2))
        tmax = min(tmax, max(t1, t2))

        result = select(int32x16(1), int32x16(0), tmax > max(tmin, float32x16(0)))
        hit_min_t = max(tmin, float32x16(0))

        for i in range(16):
            if result[i] == 1:  # ray intersect bbox
                if bvh16_node.is_leaf[i]:
                    if bvh16_node.triangles[i] >= 0:
                        if isect_bool_shape_triangle(ray, shape, isec_dist, bvh16_node.triangles[i]):
                            return 1
                else:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = bvh16_node.children[i]
                    obj.min_t = hit_min_t[i]

    return 0
