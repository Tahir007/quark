
import os.path


def save_png_image(buffer, width, height, fname):
    def write_raw_png(buf, width, height):
        import zlib
        import struct
        width_byte_4 = width * 4
        raw_data = b"".join(
            b'\x00' + buf[span:span + width_byte_4] for span in range((height - 1) * width * 4, -1, - width_byte_4))

        def png_pack(png_tag, data):
            chunk_head = png_tag + data
            return struct.pack("!I", len(data)) + chunk_head + struct.pack("!I", 0xFFFFFFFF & zlib.crc32(chunk_head))

        return b"".join([
            b'\x89PNG\r\n\x1a\n',
            png_pack(b'IHDR', struct.pack("!2I5B", width, height, 8, 6, 0, 0, 0)),
            png_pack(b'IDAT', zlib.compress(raw_data, 9)),
            png_pack(b'IEND', b'')])

    def write_png(color_buffer, width, height, filename):
        # RGBA format
        buf = bytearray()
        for y in range(height):
            for x in range(width):
                p = color_buffer[y * width + x]
                r = min(255, int(p[0] * 255.99))
                g = min(255, int(p[1] * 255.99))
                b = min(255, int(p[2] * 255.99))
                buf.append(r)
                buf.append(g)
                buf.append(b)
                buf.append(255)

        data = write_raw_png(buf, width, height)
        with open(filename, 'wb') as fp:
            fp.write(data)

    write_png(buffer, width, height, fname)


def save_image(buffer, width, height, fname):
    root, ext = os.path.splitext(fname)
    if ext == '.png':
        save_png_image(buffer, width, height, fname)

