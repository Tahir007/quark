
from simdy import simdy_kernel, float32x3, float32, int32
from .params import HitPoint, LightSample, Params, Light, Ray, AreaSample, TriangleData
from .isec import isect_bool_scene
from .core import sample_point_on_mesh, calculate_pdf_on_mesh


@simdy_kernel
def sample_point_light(hp: HitPoint, light: Light, ls: LightSample):
    wi = light.position - hp.hit
    distance_squared = dot(wi, wi)
    distance = sqrt(distance_squared)
    wi = wi / float32x3(distance)
    if dot(hp.wo, hp.normal) * dot(hp.normal, wi) > 0.0:
        ls.intensity = light.intensity
        ls.distance = distance
        ls.pdf = distance_squared
        ls.wi = wi
        return 1
    return 0


@simdy_kernel
def sample_area_light(hp: HitPoint, light: Light, params: Params, ls: LightSample):

    valid = 0
    sample = AreaSample()
    if light.shp_type == 0:  # rectangle
        rect = params.light_rectangles[light.shp_id]
        sample.position = rect.p + rect.edge_a * random_float32() + rect.edge_b * random_float32()
        sample.pdf_a = rect.inv_area
        sample.normal = rect.normal
        valid = 1
    elif light.shp_type == 1:  # mesh
        mesh = params.light_meshes[light.shp_id]
        triangle_idx = int32(float32(mesh.ntriangles) * random_float32())
        r1 = sqrt(random_float32())
        u = 1.0 - r1
        v = random_float32() * r1
        sample = AreaSample()
        valid = sample_point_on_mesh(mesh, u, v, triangle_idx, sample)

    if valid == 0:
        return valid

    wi = hp.hit - sample.position
    distance_squared = dot(wi, wi)
    distance = sqrt(distance_squared)
    wi = wi / float32x3(distance)
    if dot(hp.wo, hp.normal) * dot(hp.normal, -wi) > 0.0:
        cos_normal = dot(float32x3(sample.normal), wi)
        if cos_normal > float32(1e-6):
            ls.pdf = sample.pdf_a * distance_squared / cos_normal
            ls.wi = -wi
            ls.distance = distance
            ls.intensity = light.intensity
            return 1

    return 0


@simdy_kernel
def sample_light(hp: HitPoint, params: Params, ls: LightSample):
    nlights = len(params.lights)
    if nlights == 0:
        return 0

    light_id = int32(float32(nlights) * random_float32())
    lprop = params.lights[light_id]
    valid = 0
    if lprop.type == 0:  # point light
        valid = sample_point_light(hp, lprop, ls)
        ls.delta_light = 1
    elif lprop.type == 1:  # area light
        valid = sample_area_light(hp, lprop, params, ls)
        ls.delta_light = 0

    if valid:
        ls.pdf *= 1.0 / float32(nlights)
        ls.intensity = ls.intensity * (1.0 / ls.pdf)

        ray = Ray()
        ray.origin = hp.hit
        ray.direction = ls.wi
        epsilon = float32(0.001)
        hit = isect_bool_scene(ray, params, ls.distance - epsilon)
        if hit:
            valid = 0

    return valid


@simdy_kernel
def emission(hp: HitPoint, params: Params):
    if hp.light_id < 0:
        return float32x3(0)

    light = params.lights[hp.light_id]

    if dot(hp.normal, hp.wi * -1.0) > float32(1e-6):
        return light.intensity
    return float32x3(0)


@simdy_kernel
def pdf_area_light(hp: HitPoint, light: Light, params: Params):

    pdf = float32(0)
    if light.shp_type == 0:  # rectangle
        rect = params.light_rectangles[light.shp_id]
        wi = -hp.wi
        if dot(hp.normal, wi) > float32(1e-6):
            pdf = rect.inv_area * hp.t * hp.t / dot(hp.normal, wi)
    elif light.shp_type == 1:  # mesh
        mesh = params.light_meshes[light.shp_id]
        wi = -hp.wi
        if dot(hp.normal, wi) > float32(1e-6):
            pdf_a = calculate_pdf_on_mesh(mesh, hp.light_tri_idx)
            pdf = pdf_a * hp.t * hp.t / dot(hp.normal, wi)

    return pdf


@simdy_kernel
def pdf_light(hp: HitPoint, params: Params):
    if hp.light_id < 0:
        return float32(0)

    pdf = float32(0.0)

    light = params.lights[hp.light_id]
    if light.type == 1:  # area light
        pdf = pdf_area_light(hp, light, params)
        pdf *= 1.0 / float32(len(params.lights))

    return pdf
