
from simdy import simdy_kernel, int32, struct, register_user_type, float32x3, float32
from .params import array_BvhNode, Shape, TriangleData, Ray
from .core import isect_bool_shape_triangle, isect_shape_triangle


class BvhBuildEntry(struct):
    __slots__ = ['parent', 'start', 'end']

    def __init__(self):
        self.parent = int32()
        self.start = int32()
        self.end = int32()


register_user_type(BvhBuildEntry, factory=BvhBuildEntry)


class BBox(struct):
    __slots__ = ['p0', 'p1']

    def __init__(self):
        self.p0 = float32x3()
        self.p1 = float32x3()


register_user_type(BBox, factory=BBox)


@simdy_kernel
def _create_bvh_nodes(shape: Shape, nodes: array_BvhNode) -> int32:
    def calc_tri_bbox(faces, vertices, index, bbox):
        face_idx = index * 9
        p0 = vertices[faces[face_idx]]
        p1 = vertices[faces[face_idx + 1]]
        p2 = vertices[faces[face_idx + 2]]
        eps = float32x3(0.0001)
        bbox.p0 = min(min(p0, p1), p2) - eps
        bbox.p1 = max(max(p0, p1), p2) + eps

        centroid = (p0 + p1 + p2) * 0.333333333333
        return centroid


    def calc_tri_centroid(faces, vertices, index):
        face_idx = index * 9
        p0 = vertices[faces[face_idx]]
        p1 = vertices[faces[face_idx + 1]]
        p2 = vertices[faces[face_idx + 2]]
        centroid = (p0 + p1 + p2) * 0.333333333333
        return centroid


    def swap_triangles(faces, index1, index2):
        i = index1 * 9
        j = index2 * 9
        for k in range(9):
            tmp1 = faces[i + k]
            tmp2 = faces[j + k]
            faces[i + k] = tmp2
            faces[j + k] = tmp1

    leaf_size = 1
    n_leafs = 0
    ntriangles = shape.ntriangles
    faces = shape.faces
    vertices = shape.verts

    bbox = BBox()
    build_stack = array(BvhBuildEntry, 128)
    untouched = 0x7fffffff
    touched_twice = 0x7ffffffd
    root_parent = 0x7ffffffc
    cur_node = 0

    # push the root on stack
    stack_ptr = 0
    obj = build_stack[stack_ptr]
    obj.start = 0
    obj.end = ntriangles
    obj.parent = root_parent
    stack_ptr += 1

    while stack_ptr > 0:
        stack_ptr -= 1
        obj = build_stack[stack_ptr]
        start = obj.start
        end = obj.end
        nprims = end - start
        right_offset = untouched

        # calculate bbox for this node
        centroid = calc_tri_bbox(faces, vertices, start, bbox)
        min_bb = bbox.p0
        max_bb = bbox.p1
        min_bc = centroid
        max_bc = centroid

        start_idx = start + 1
        while start_idx < end:
            centroid = calc_tri_bbox(faces, vertices, start_idx, bbox)
            min_bb = min(min_bb, bbox.p0)
            max_bb = max(max_bb, bbox.p1)
            min_bc = min(min_bc, centroid)
            max_bc = max(max_bc, centroid)
            start_idx += 1

        # if number of primitives less then leaf size then this becomes leaf
        # Signified by right_offset == 0
        if nprims <= leaf_size:
            right_offset = 0
            n_leafs += 1

        # fill data for current node
        node = nodes[cur_node]
        node.bbox_p0 = min_bb
        node.bbox_p1 = max_bb
        node.n_prims = nprims
        node.start = start
        node.right_offset = right_offset
        cur_node += 1

        # Don't do this for root node
        if obj.parent != root_parent:
            node = nodes[obj.parent]
            node.right_offset -= 1

            # when this is second touch, this is right child
            if node.right_offset == touched_twice:
                node.right_offset = cur_node - 1 - obj.parent

        if right_offset == 0:  # this is leaf no need to subdivide
            continue

        # split dimension
        extent = max_bc - min_bc
        split_dim = 0
        split_coord = 0.5 * (min_bc[0] + max_bc[0])
        if extent[1] > extent[0]:
            split_dim = 1
            split_coord = 0.5 * (min_bc[1] + max_bc[1])
        if extent[2] > extent[1]:
            split_dim = 2
            split_coord = 0.5 * (min_bc[2] + max_bc[2])

        # partition list of objects on this split
        mid = start
        idx = start
        while idx < end:
            centroid = calc_tri_centroid(faces, vertices, idx)
            cen = centroid[split_dim]
            if cen < split_coord:
                swap_triangles(faces, idx, mid)
                mid += 1
            idx += 1

        # we got bad split and we choose center
        if mid == start or mid == end:
            mid = start + (end - start) / 2

        # push right child on stack
        obj = build_stack[stack_ptr]
        obj.start = mid
        obj.end = end
        obj.parent = cur_node - 1
        stack_ptr += 1

        # push left child on stack
        obj = build_stack[stack_ptr]
        obj.start = start
        obj.end = mid
        obj.parent = cur_node - 1
        stack_ptr += 1

    return cur_node


@simdy_kernel
def _copy_nodes(in_nodes: array_BvhNode, out_nodes: array_BvhNode, n: int32):
    for i in range(n):
        in_node = in_nodes[i]
        p0 = in_node.bbox_p0
        p1 = in_node.bbox_p1
        n_prims = in_node.n_prims
        start = in_node.start
        r_offset = in_node.right_offset

        out_node = out_nodes[i]
        out_node.bbox_p0 = p0
        out_node.bbox_p1 = p1
        out_node.n_prims = n_prims
        out_node.start = start
        out_node.right_offset = r_offset


def create_bvh_tree_nodes(shape):

    tmp_nodes = array_BvhNode(size=shape.ntriangles*2)
    n = _create_bvh_nodes(shape, tmp_nodes)
    nodes = array_BvhNode(size=n)
    _copy_nodes(tmp_nodes, nodes, int32(n))

    return nodes


class BvhTraversal(struct):
    __slots__ = ['node_idx', 'min_t']

    def __init__(self):
        self.node_idx = int32()
        self.min_t = float32()


register_user_type(BvhTraversal, factory=BvhTraversal)


@simdy_kernel
def isect_mesh_bvh(ray: Ray, shape: Shape, max_dist: float32, tdata: TriangleData):

    isec_dist = max_dist
    hit_ocur = 0
    bvh_nodes = shape.nodes

    origin = ray.origin
    inv_ray_dir = float32x3(1.0) / ray.direction

    traversal_stack = array(BvhTraversal, 64)
    stack_ptr = 0

    # push root node on the stack
    obj = traversal_stack[stack_ptr]
    obj.node_idx = 0
    obj.min_t = float32(-1e30)

    while stack_ptr >= 0:
        obj = traversal_stack[stack_ptr]
        n_idx = obj.node_idx
        near = obj.min_t
        stack_ptr -= 1

        if near > isec_dist:
            continue

        node = bvh_nodes[n_idx]
        right_offset = node.right_offset
        if right_offset == 0:  # this is leaf
            start_idx = node.start
            for i in range(node.n_prims):
                tri_idx = start_idx + i
                if isect_shape_triangle(ray, shape, isec_dist, tri_idx, tdata):
                    isec_dist = tdata.t
                    hit_ocur = 1
        else:  # inner node
            left_idx = n_idx + 1
            node = bvh_nodes[left_idx]
            t1 = (node.bbox_p0 - origin) * inv_ray_dir
            t2 = (node.bbox_p1 - origin) * inv_ray_dir
            tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
            tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
            hit1 = 0
            if tmax > max(tmin, float32(0.0)):
                hit1 = 1
                hit_min_t1 = max(tmin, float32(0.0))

            right_idx = n_idx + right_offset
            node = bvh_nodes[right_idx]
            t1 = (node.bbox_p0 - origin) * inv_ray_dir
            t2 = (node.bbox_p1 - origin) * inv_ray_dir
            tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
            tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
            hit2 = 0
            if tmax > max(tmin, float32(0.0)):
                hit2 = 1
                hit_min_t2 = max(tmin, float32(0.0))

            if hit1 == 1 and hit2 == 1:  # both inner nodes are hit
                if hit_min_t2 < hit_min_t1:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = right_idx
                    obj.min_t = hit_min_t2
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = left_idx
                    obj.min_t = hit_min_t1
                else:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = left_idx
                    obj.min_t = hit_min_t1
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = right_idx
                    obj.min_t = hit_min_t2
            else:
                if hit1:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = left_idx
                    obj.min_t = hit_min_t1

                if hit2:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = right_idx
                    obj.min_t = hit_min_t2

    return hit_ocur


@simdy_kernel
def isect_bool_mesh_bvh(ray: Ray, shape: Shape, max_dist: float32):

    isec_dist = max_dist
    bvh_nodes = shape.nodes

    origin = ray.origin
    inv_ray_dir = float32x3(1.0) / ray.direction

    traversal_stack = array(BvhTraversal, 64)
    stack_ptr = 0

    # push root node on the stack
    obj = traversal_stack[stack_ptr]
    obj.node_idx = 0
    obj.min_t = float32(-1e30)

    while stack_ptr >= 0:
        obj = traversal_stack[stack_ptr]
        n_idx = obj.node_idx
        near = obj.min_t
        stack_ptr -= 1

        if near > isec_dist:
            continue

        node = bvh_nodes[n_idx]
        right_offset = node.right_offset
        if right_offset == 0:  # this is leaf
            start_idx = node.start
            for i in range(node.n_prims):
                tri_idx = start_idx + i
                if isect_bool_shape_triangle(ray, shape, isec_dist, tri_idx):
                    return 1
        else:  # inner node
            left_idx = n_idx + 1
            node = bvh_nodes[left_idx]
            t1 = (node.bbox_p0 - origin) * inv_ray_dir
            t2 = (node.bbox_p1 - origin) * inv_ray_dir
            tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
            tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
            hit1 = 0
            if tmax > max(tmin, float32(0.0)):
                hit1 = 1
                hit_min_t1 = max(tmin, float32(0.0))

            right_idx = n_idx + right_offset
            node = bvh_nodes[right_idx]
            t1 = (node.bbox_p0 - origin) * inv_ray_dir
            t2 = (node.bbox_p1 - origin) * inv_ray_dir
            tmin = max(max(min(t1[0], t2[0]), min(t1[1], t2[1])), min(t1[2], t2[2]))
            tmax = min(min(max(t1[0], t2[0]), max(t1[1], t2[1])), max(t1[2], t2[2]))
            hit2 = 0
            if tmax > max(tmin, float32(0.0)):
                hit2 = 1
                hit_min_t2 = max(tmin, float32(0.0))

            if hit1 == 1 and hit2 == 1:  # both inner nodes are hit
                if hit_min_t2 < hit_min_t1:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = right_idx
                    obj.min_t = hit_min_t2
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = left_idx
                    obj.min_t = hit_min_t1
                else:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = left_idx
                    obj.min_t = hit_min_t1
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = right_idx
                    obj.min_t = hit_min_t2
            else:
                if hit1:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = left_idx
                    obj.min_t = hit_min_t1

                if hit2:
                    stack_ptr += 1
                    obj = traversal_stack[stack_ptr]
                    obj.node_idx = right_idx
                    obj.min_t = hit_min_t2

    return 0
