from math import sqrt
from simdy import simdy_kernel, float32x3, int32, float32
from .params import Ray, Shape, TriangleData, AreaSample


@simdy_kernel
def isect_shape_triangle(ray: Ray, shape: Shape, max_dist: float32, index: int32, tdata: TriangleData):
    faces = shape.faces
    verts = shape.verts

    idx = index * 9
    face0 = faces[idx]
    face1 = faces[idx + 1]
    face2 = faces[idx + 2]
    p0 = verts[face0]
    p1 = verts[face1]
    p2 = verts[face2]
    e1 = p1 - p0
    e2 = p2 - p0

    d = ray.direction

    p = float32x3(d[1] * e2[2] - d[2] * e2[1], d[2] * e2[0] - d[0] * e2[2], d[0] * e2[1] - d[1] * e2[0])
    a = dot(e1, p)
    if a > -0.00001 and a < 0.00001:
        return 0

    f = 1.0 / a
    s = ray.origin - p0
    u = f * dot(s, p)
    if u < 0.0 or u > 1.0:
        return 0
    q = float32x3(s[1] * e1[2] - s[2] * e1[1], s[2] * e1[0] - s[0] * e1[2], s[0] * e1[1] - s[1] * e1[0])
    v = f * dot(d, q)
    if v < 0.0 or u + v > 1.0:
        return 0

    t = f * dot(e2, q)
    if t > 0.0001 and t < max_dist:
        tdata.t = t
        tdata.u = u
        tdata.v = v
        tdata.index = index
        return 1

    return 0



@simdy_kernel
def isect_bool_shape_triangle(ray: Ray, shape: Shape, max_dist: float32, index: int32):
    faces = shape.faces
    verts = shape.verts

    idx = index * 9
    face0 = faces[idx]
    face1 = faces[idx + 1]
    face2 = faces[idx + 2]
    p0 = verts[face0]
    p1 = verts[face1]
    p2 = verts[face2]
    e1 = p1 - p0
    e2 = p2 - p0

    d = ray.direction

    p = float32x3(d[1] * e2[2] - d[2] * e2[1], d[2] * e2[0] - d[0] * e2[2], d[0] * e2[1] - d[1] * e2[0])
    a = dot(e1, p)
    if a > -0.00001 and a < 0.00001:
        return 0

    f = 1.0 / a
    s = ray.origin - p0
    u = f * dot(s, p)
    if u < 0.0 or u > 1.0:
        return 0
    q = float32x3(s[1] * e1[2] - s[2] * e1[1], s[2] * e1[0] - s[0] * e1[2], s[0] * e1[1] - s[1] * e1[0])
    v = f * dot(d, q)
    if v < 0.0 or u + v > 1.0:
        return 0

    t = f * dot(e2, q)
    if t > 0.0001 and t < max_dist:
        return 1

    return 0


@simdy_kernel
def calculate_normal(shape: Shape, u: float32, v: float32, idx: int32) -> float32x3:
    faces = shape.faces
    if len(shape.normals) > 0:
        normals = shape.normals
        idx = idx * 9
        n0 = normals[faces[idx + 3]]
        n1 = normals[faces[idx + 4]]
        n2 = normals[faces[idx + 5]]
        normal = n0 * float32(1.0 - u - v) + u * n1 + v * n2
        normal = normal / float32x3(sqrt(dot(normal, normal)))  # normalization of vector
    else:
        verts = shape.verts
        idx = idx * 9
        face0 = faces[idx]
        face1 = faces[idx + 1]
        face2 = faces[idx + 2]
        p0 = verts[face0]
        p1 = verts[face1]
        p2 = verts[face2]
        e1 = p1 - p0
        e2 = p2 - p0
        p = float32x3(e1[1] * e2[2] - e1[2] * e2[1], e1[2] * e2[0] - e1[0] * e2[2], e1[0] * e2[1] - e1[1] * e2[0])
        normal = float32x3(p / float32x3(sqrt(dot(p, p))))  # normalization of vector
    return normal


@simdy_kernel
def calculate_position_and_pdf_on_mesh(shape: Shape, u: float32, v: float32, idx: int32, sample: AreaSample):
    faces = shape.faces
    verts = shape.verts
    idx = idx * 9
    face0 = faces[idx]
    face1 = faces[idx + 1]
    face2 = faces[idx + 2]
    p0 = verts[face0]
    p1 = verts[face1]
    p2 = verts[face2]
    e1 = p1 - p0
    e2 = p2 - p0
    sample.position = p0 * float32(1.0 - u - v) + u * p1 + v * p2
    normal = float32x3(e1[1] * e2[2] - e1[2] * e2[1], e1[2] * e2[0] - e1[0] * e2[2], e1[0] * e2[1] - e1[1] * e2[0])
    area = sqrt(dot(normal, normal)) * 0.5
    sample.pdf_a = float32(1.0) / (area * float32(shape.ntriangles))


@simdy_kernel
def calculate_pdf_on_mesh(mesh: Shape, idx: int32) -> float32:
    faces = mesh.faces
    verts = mesh.verts
    idx = idx * 9
    face0 = faces[idx]
    face1 = faces[idx + 1]
    face2 = faces[idx + 2]
    p0 = verts[face0]
    p1 = verts[face1]
    p2 = verts[face2]
    e1 = p1 - p0
    e2 = p2 - p0
    normal = float32x3(e1[1] * e2[2] - e1[2] * e2[1], e1[2] * e2[0] - e1[0] * e2[2], e1[0] * e2[1] - e1[1] * e2[0])
    area = sqrt(dot(normal, normal)) * 0.5
    pdf_a = float32(1.0) / (area * float32(mesh.ntriangles))
    return pdf_a


@simdy_kernel
def sample_point_on_mesh(shape: Shape, u: float32, v: float32, idx: int32, sample: AreaSample) -> int32:
    sample.normal = calculate_normal(shape, u, v, idx)
    calculate_position_and_pdf_on_mesh(shape, u, v, idx, sample)
    return 1

