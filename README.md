## Path tracer using SIMDy

Quark is path tracer developed with Python using SIMDy package. SIMDy is used for writing compute
intensive kernels and path tracer is perfect for showing flexibility and speed of SIMDy kernels. Idea
here is to develop very scalable path tracer that uses heavily AVX-512. i9-7980XE has more than
one teraflops of computing power! To make good use of AVX-512 you must carefully design your
data structures. If you succeed in this on lots of places where you previously force to do some sorts
caching like for example in filters (Tent, Mitchell, Gaussian, etc.) now is not needed
because it is cheap to compute it directly. This approach simplifies rendering algorithm. Of course
there are still places where caching is good approach but this is reduced now.


![picture](scenes/cornell_10k.png)

### Features

* Uses AVX-512, AVX2, AVX, SSE, FMA instruction sets
* Uses all CPU cores in parallel
* Supports iterative rendering
* Supports OBJ
* Uses BVH/BVH16 trees to accelerate ray intersection tests
* json - for scene description
* filters - Box, Triangle, Gaussian, Mitchell
* Area lights
* Tone mapping

Example of scene description file

```json
{
    "global":{
        "resolution": [512, 512],
        "samples_per_pixel": 32,
        "output": "cornell.png",
        "tonemap": "filmic"
    },
    "camera":{
        "eye": [0.0, 1.0, 4.0],
        "lookat": [0.0, 1.0, -1.0],
        "fov": 40
    },
    "shapes":
    [
        {
            "fname": "CornellBox-Original.obj"
        }
    ]
}

```

Usage:
python main.py path_to_json